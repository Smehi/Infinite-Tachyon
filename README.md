# Infinite Tachyon

IT is a 1v1 local multiplayer RTS game. Made with a team of 5 over the course of about 5 months. For more info see my [site](https://www.semihorhan.nl).

## Getting the project

### Recommended
Open Bash
- `git clone https://gitlab.com/Smehi/Infinite-Tachyon.git`
- `cd Infinite-Tachyon`
- `git lfs pull`

### Alternative 
If you clone this repo from a GUI like SourceTree or Kraken... just wait. It might seem like it's stuck but you just have to be patient.
After cloning the repo you still need the LFS files. Your GUI should prompt you about this otherwise you won't be able to scan the project.

### Note
Downloading the .zip will not work because it doesn't have the lfs object files and you can't get it either because it's not a git repo.