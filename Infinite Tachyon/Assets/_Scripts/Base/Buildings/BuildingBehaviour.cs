﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BuildingBehaviour : MonoBehaviour
{

    private int currentState;

    const int BUILDING_STATE = 0;
    const int BUILD_STATE = 1;

    [SerializeField]
    private Color transparentColor;
    private Color nonTransparentColor;

    public bool isSelectable = false;
    private MeshRenderer buildingRend;

    private void Start()
    {
        buildingRend = GetComponent<MeshRenderer>();
        currentState = BUILDING_STATE;
        nonTransparentColor = new Color(buildingRend.material.color.r, buildingRend.material.color.g, buildingRend.material.color.b, 1);
        transparentColor = new Color(buildingRend.material.color.r, buildingRend.material.color.g, buildingRend.material.color.b, 0.5f);
    }

    void FixedUpdate () {
        switch (currentState)
        {
            case BUILDING_STATE:
                buildingRend.material.color = transparentColor;
                StartCoroutine(Build(1f));
                break;

            case BUILD_STATE:
                buildingRend.material.color = nonTransparentColor;
                isSelectable = true;
                break;

            default:
                break;
        }
    }

    IEnumerator Build(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        currentState = BUILD_STATE;
    }
}
