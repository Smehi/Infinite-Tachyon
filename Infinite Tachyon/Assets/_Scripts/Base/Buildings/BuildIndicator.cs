﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BuildIndicator : MonoBehaviour
{
    public Material placeableMat;
    public Material notPlaceableMat;

    private MeshRenderer mesh;
    private List<GameObject> collidedCells = new List<GameObject>();
    private Vector3 lastPosition = Vector3.zero;
    public bool isBuildable = false;

    private PlayerResources resScript;
    public string name;
    public List<Building> buildings;
    private BaseConnection baseConnection;
    private float energyCost;
    private float ITCost;

    private List<GameObject> obstructions = new List<GameObject>();

    //This is to show where the player is building and if its a valid location

    private void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        resScript = LobbyManager.instance.localPlayer.transform.GetComponent<PlayerResources>();
        baseConnection = LobbyManager.instance.localPlayer.transform.GetComponent<BaseConnection>();
        buildings = baseConnection.GetBuildings;
        if(name != "")
        {
            energyCost = baseConnection.SearchForBuilding(name, buildings).energyRequired;
            ITCost = baseConnection.SearchForBuilding(name, buildings).IT_required;
        }        
    }

    //Checks the cells the player wants to build on and if there are other obstuctions in the way
    private void FixedUpdate()
    {
        foreach (GameObject cell in collidedCells)
        {
            if (!cell.GetComponent<CellInit>().isPlaceable || obstructions.Count > 0 || collidedCells.Count == 0 || !resScript.HasSufficientEnergy(energyCost))
            {
                mesh.material = notPlaceableMat;
                isBuildable = false;
                break;
            }
            else
            {
                mesh.material = placeableMat;
                isBuildable = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "GridCell")
        {
            collidedCells.Add(other.gameObject);
        }
        else
        {
            obstructions.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "GridCell")
        {
            collidedCells.Remove(other.gameObject);
        }
        else
        {
            obstructions.Remove(other.gameObject);
        }
    }
}
