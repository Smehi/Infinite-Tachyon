﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

//Checks player input to place the building at the right location
public class BuildInputs : NetworkBehaviour
{

    private GameObject currentBuilding;
    private string currentBuildingName;

    public GameObject docks;
    public GameObject airfield;
    public GameObject techlab;
    public GameObject powerGenerator;

    private BuildIndicator currentBuildingInit;
    
    private BaseConnection baseScript;
    private SpawnUnitsUI spawnUnitsUI;

    private Vector3 airfieldOffset;

    private List<Building> buildings;
    private Building incomingBuilding;
    private List<GameObject> buildingInProgress = new List<GameObject>();

    private Vector3 nullVector = new Vector3(0, 0, 0);
    private Vector3 pos;

    // Slight delay on start so everything on the network is setup befor start happens
    void Start()
    {
        Invoke("DelayedStart", .2f);
    }

    void DelayedStart()
    {

        if (GetComponent<NetworkIdentity>().hasAuthority)
        {
            spawnUnitsUI = LobbyManager.instance.localPlayer.transform.GetComponent<SpawnUnitsUI>();
            baseScript = LobbyManager.instance.localPlayer.transform.GetComponent<BaseConnection>();
            buildings = baseScript.GetBuildings;            
        }
        else
        {
            spawnUnitsUI = LobbyManager.instance.notLocalPlayer.transform.GetComponent<SpawnUnitsUI>();
            baseScript = LobbyManager.instance.notLocalPlayer.transform.GetComponent<BaseConnection>();
            buildings = baseScript.GetBuildings;            
        }
    }

    void Update()
    {
        if (currentBuilding != null && currentBuildingName != null)
        {
            incomingBuilding = baseScript.SearchForBuilding(currentBuildingName, buildings);
            pos = GetBuildPosition();

            if (pos != nullVector)
            {
                currentBuilding.transform.position = GetBuildPosition();
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (currentBuildingInit.isBuildable)
                {
                    if (pos != nullVector)
                    {
                        buildingInProgress.Remove(currentBuilding);
                        Destroy(currentBuilding);

                        transform.parent.GetComponent<BaseConnection>().CmdSpawnBuilding(currentBuildingName, pos.x, pos.y+0.8f, pos.z, true);
                        ActivateButton(currentBuildingName);

                        currentBuilding = null;
                    }
                }          
            }

            if (Input.GetMouseButtonDown(1))
            {
                Destroy(currentBuilding);
                currentBuilding = null;
            }

            if (incomingBuilding != null)
            {
                incomingBuilding.cooldownImage.fillAmount = incomingBuilding.cooldown.x / incomingBuilding.cooldown.y;
            }


        }
    }

    // Gets the position to build from the grid
    public Vector3 GetBuildPosition()
    {
        RaycastHit hit;
        Vector3 point = new Vector3();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        LayerMask grid = LayerMask.GetMask("Grid");
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, grid))
        {
            if (hit.transform.root.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                point = hit.transform.position;
            }
        }

        return point;
    }

    //Sets the building that needs to be build with the building name
    public void SpawnBuilding(string name)
    {
        
        if (currentBuilding == null)
        {
            currentBuilding = Instantiate(baseScript.SearchForBuilding(name, buildings).construction);
            buildingInProgress.Add(currentBuilding);
        }

        if (currentBuilding != null)
        {
            currentBuildingInit = currentBuilding.GetComponentInChildren<BuildIndicator>();
            currentBuildingInit.name = name;
            currentBuildingName = name;
        }
    }

    //Activates UI for the buildings the player unlocks after building a building
    public void ActivateButton(string name)
    {
        if (name == "PowerGenerator")
        {
            spawnUnitsUI.docksButton.SetActive(true);
        }
        else if (name == "Docks")
        {
            spawnUnitsUI.airfieldButton.SetActive(true);
        }
        else if (name == "Airfield")
        {
            spawnUnitsUI.techlabButton.SetActive(true);
        }
        else if (name == "Techlab")
        {
            spawnUnitsUI.airSpecialTab.SetActive(true);
            spawnUnitsUI.groundSpecialUI.SetActive(true);
        }
    }

}
