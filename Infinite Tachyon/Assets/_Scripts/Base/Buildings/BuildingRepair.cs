﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class BuildingRepair : MonoBehaviour
{
    [SerializeField] private LayerMask repairLayerMask;
    [SerializeField] private bool lerpRepair;
    [SerializeField] private int healthPerSecond;

    private bool inRepairMode;
    private Text repairText;
    private UnitSelectManager unitSelectManager;
    private Camera cam;
    private RaycastHit hit;

    private Transform hitParent;
    private BuildingHealth buildingHealth;
    private string hitName;

    private void Start()
    {
        // Start the initialization 1 second after ready because of the headquaters isn't in the right spot
        Invoke("Init", 1f);
    }

    // Use this for initialization
    private void Init()
    {
        inRepairMode = false;

        unitSelectManager = transform.root.GetComponent<UnitSelectManager>();
        cam = transform.root.GetComponentInChildren<Camera>();

        repairText = transform.root.Find("UnitCanvas/Repair Text").GetComponent<Text>();
        Button repairButton = transform.root.Find("UnitCanvas/HeadquartersUI/Repair Building").GetComponent<Button>();
        repairButton.onClick.AddListener(StartRepairMode);
    }

    // Update is called once per frame
    private void Update()
    {
        // We always call the update because the script has to be on, but we only go in it when we are in the repair mode
        if (inRepairMode)
        {
            // To exit repair mode
            if (Input.GetMouseButtonDown(1) || Input.GetAxisRaw("Cancel") > 0)
            {
                StopRepairMode();
                return;
            }

            // We make a new ray every frame because the camera/mouse can move
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, repairLayerMask))
            {
                // We get the parent of what we hit because the selection colliders are in a child object
                hitParent = hit.collider.transform.parent.parent;
                if (hitParent.GetComponent<NetworkIdentity>().hasAuthority)
                {
                    buildingHealth = hitParent.GetComponent<BuildingHealth>();
                    hitName = hitParent.name;

                    repairText.text = $"{hitName} health {buildingHealth.Health} >>> {buildingHealth.MaxHealth}";
                }
            }
            else
            {
                buildingHealth = null;
                repairText.text = "";
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, Mathf.Infinity, repairLayerMask))
                {
                    // Send a command to the server telling it we want to repair our building
                    buildingHealth.CmdRepairBuilding(lerpRepair, healthPerSecond);
                    return;
                }
            }
        }
    }

    private void ActivateBuildingHealthbars(bool state)
    {
        foreach (GameObject building in unitSelectManager.ExistingBuildings)
        {
            building.GetComponent<BuildingHealth>().ActivateHealthBar(state);
        }
    }

    public void StartRepairMode()
    {
        unitSelectManager.ClearSelection();

        ActivateBuildingHealthbars(true);
        repairText.gameObject.SetActive(true);
        inRepairMode = true;

        // We want to disable the UnitSelectManager for the time being because we aren't going to be using it
        unitSelectManager.enabled = false;
    }

    private void StopRepairMode()
    {
        // Enable UnitSelectManager again so we can select and move units again
        unitSelectManager.enabled = true;

        ActivateBuildingHealthbars(false);
        repairText.text = "";

        inRepairMode = false;

        foreach (GameObject building in unitSelectManager.ExistingBuildings)
        {
            building.GetComponent<BuildingHealth>().ActivateHealthBar(false);
        }
    }
}
