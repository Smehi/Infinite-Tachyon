﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//This script is for initialising buildings and start building

public class BuildingInit : NetworkBehaviour
{
    [SyncVar] private float buildTime;
    [SyncVar] private float IT_required;
    [SyncVar] private float energyRequired;

    private PlayerResources resScript;
    float decreaseValue;
    private BaseInit baseInit;
    private Transform parent;
    private BaseConnection baseConnection;
    private bool parentSet;

    public GameObject visionObj;
    public GameObject buildingAnimation;

    private AudioManager audioManager;
    private AudioSource audioSource;

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        if (visionObj != null)
        {
            visionObj.SetActive(true);
        }
        else
        {
            visionObj = transform.GetChild(0).Find("UnitVision").gameObject;
            visionObj.SetActive(true);
        }
    }

    private void Start()
    {
        if (LobbyManager.instance.localPlayer.GetComponent<NetworkIdentity>().isServer)
        {
            Invoke("AddBuildingLate", 0.25f);
        }

        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        if (audioManager == null)   
        {
            audioManager = FindObjectOfType<AudioManager>();
        }

        if (audioSource == null)    
        {
            audioSource = this.gameObject.AddComponent<AudioSource>();
        }

        audioManager.StartCoroutine(audioManager.Play("BuildingBuild", audioSource));
    }

    private void AddBuildingLate()
    {
        LobbyManager.instance.localPlayer.GetComponent<BaseConnection>().AddBuildingToTrackList(this.gameObject);

    }
    // sets the building under the correct PlayerConnections base
    private void SetParent()
    {
        if (GetComponent<NetworkIdentity>().hasAuthority)
        {
            Transform localPlayer = LobbyManager.instance.localPlayer.transform;
            baseConnection = localPlayer.gameObject.GetComponent<BaseConnection>();

            if (baseConnection.CellsAlive)
            {
                for (int i = 0; i < localPlayer.transform.childCount; i++)
                {
                    if (localPlayer.transform.GetChild(i).CompareTag("MainBase"))
                    {
                        parent = localPlayer.transform.GetChild(i);
                        this.transform.SetParent(parent);
                        break;
                    }
                }
            }

            resScript = localPlayer.GetComponent<PlayerResources>();
        }
        else
        {
            Transform notLocalPlayer = LobbyManager.instance.notLocalPlayer.transform;
            baseConnection = notLocalPlayer.gameObject.GetComponent<BaseConnection>();

            if (baseConnection.CellsAlive)
            {
                for (int i = 0; i < notLocalPlayer.transform.childCount; i++)
                {
                    if (notLocalPlayer.transform.GetChild(i).CompareTag("MainBase"))
                    {
                        parent = notLocalPlayer.transform.GetChild(i);
                        this.transform.SetParent(parent);
                        break;
                    }
                }
            }

            resScript = LobbyManager.instance.notLocalPlayer.transform.GetComponent<PlayerResources>();
        }

        if (BuildTime > 0 && !parentSet)
        {
            decreaseValue = IT_required;
            StartCoroutine(Building());
            parentSet = true;
        }
        PlaySound();
    }

    private void FixedUpdate()
    {
        if (parent == null)
        {
            SetParent();
        }
    }

    // Builds the building from the bottom up and uses resources while doing so
    public IEnumerator Building()
    {
        float maxYScale = transform.localScale.y;
        int decrement = (int)(IT_required / BuildTime);
        float scaleIncrement = ((maxYScale / BuildTime));
        WaitForSeconds wait = new WaitForSeconds(1f);
        transform.localScale = new Vector3(transform.localScale.x, 0, transform.localScale.z);
        buildingAnimation.SetActive(true);

        while (decreaseValue > 0 && resScript.AmountIt > decrement)
        {
            yield return wait;

            resScript.Decrease_IT(decrement);
            //transform.localScale += new Vector3(0, (maxYScale / BuildTime) * Time.unscaledDeltaTime, 0);

            transform.localScale += new Vector3(0, scaleIncrement, 0);
            decreaseValue -= decrement;
        }    
        
        buildingAnimation.SetActive(false);

        if (transform.localScale.y >= 1)
        {
        
            transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);
        }

        if(energyRequired < 0)
        {
            resScript.MaxEnergy += energyRequired;
        }
        else
        {
            resScript.CurrentEnergy += energyRequired;
        }
    }

    public float BuildTime
    {
        get
        {
            return buildTime;
        }
        set
        {
            buildTime = value;
        }
    }

    public float GetIT_required
    {
        get
        {
            return IT_required;
        }
        set
        {
            IT_required = value;
        }
    }

    public float EnergyRequired
    {
        get
        {
            return energyRequired;
        }
        set
        {
            energyRequired = value;
        }
    }
}