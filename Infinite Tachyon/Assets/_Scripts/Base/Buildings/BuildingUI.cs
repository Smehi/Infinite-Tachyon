﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BuildingUI : NetworkBehaviour
{
   
    public uiType type;
    public enum uiType
    {
        Ground,
        Air,
        HeadQuarters,
        none
    }

    private BaseConnection baseConnection;
    public GameObject SpawnUI;

    //Wait for the base to spawn and save the player's baseconnection
    private void Start()
    {
        Invoke("UseSingleton", 1f);
    }

    void UseSingleton()
    {

        if (GetComponent<NetworkIdentity>().hasAuthority)
        {
            baseConnection = LobbyManager.instance.localPlayer.transform.GetComponent<BaseConnection>();
            AddToList();
        }
        else
        {
            baseConnection = LobbyManager.instance.notLocalPlayer.transform.GetComponent<BaseConnection>();
            AddToList();
        }
    }

    //When a new building is build the building is added to a list of buildings with the same UI type
    private void AddToList()
    {
        switch (type)
        {
            case uiType.Ground:
                {
                    baseConnection.amountDocks.Add(gameObject);
                }
                break;
            case uiType.Air:
                {
                    baseConnection.amountAirfields.Add(gameObject);
                }
                break;
            case uiType.HeadQuarters:
                {
                    baseConnection.amountHeadquarters.Add(gameObject);
                }
                break;
        }
    }
}
