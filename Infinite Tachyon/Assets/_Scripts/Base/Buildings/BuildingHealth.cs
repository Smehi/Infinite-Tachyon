﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

public class BuildingHealth : NetworkBehaviour
{
    [SyncVar(hook = "OnChangeHealth")] [SerializeField] private int health;

    public GameObject healthBar;
    [SyncVar] private int maxHealth;

    [SerializeField] private GameObject deathParticleEffect;

    private bool hasActivatedCanvas = false;
    private Vector3 healthCanvasRotation;

    private GameObject healthCanvas;
    private UnitSelectManager unitSelectManager;
    private UnitSelect unitSelect;
    private PlayerResources resources;

    public HealthCreator healthCreator;

    private BaseConnection baseConnection;
    private NetworkIdentity localPlayerNetId;

    private void Start()
    {
        Invoke("DelayedStart", 0.2f);
    }

    //Initialize with a delay for networking to catch up 
    private void DelayedStart()
    {
        maxHealth = health;

        unitSelectManager = LobbyManager.instance.localPlayer.GetComponent<UnitSelectManager>();
        unitSelect = transform.GetComponentInChildren<UnitSelect>();

        localPlayerNetId = LobbyManager.instance.localPlayer.GetComponent<NetworkIdentity>();

        if (localPlayerNetId.isServer)
        {
            baseConnection = LobbyManager.instance.localPlayer.GetComponent<BaseConnection>();
        }

        if (!gameObject.CompareTag("Outpost"))
        {
            resources = transform.root.GetComponent<PlayerResources>();
            healthCreator.InitializeHealth(maxHealth);
        }
    }

    /// <summary>
    /// Activate the healthbar according to the state
    /// </summary>
    /// <param name="state">Active state of healthBar</param>
    public void ActivateHealthBar(bool state)
    {
        if (!state)
        {
            if (health == MaxHealth)
            {
                healthBar.SetActive(state);
            }
        }
        else
        {
            healthBar.SetActive(state);
        }
    }

    /// <summary>
    /// Repair a building by increasing its health
    /// </summary>
    /// <param name="lerp">When checked a coroutine will be called to smoothly increase the health</param>
    /// <param name="healthPerSecond">The increment of health per second</param>
    [Command]
    public void CmdRepairBuilding(bool lerp, int healthPerSecond)
    {
        if (lerp && healthPerSecond > 0)
        {
            StartCoroutine(RepairBuildingCR(health, healthPerSecond));
        }
        else
        {
            int difference = MaxHealth - health;

            health += difference;

            if (health > MaxHealth)
                health = MaxHealth;
            healthCreator.ChangeHealth(health);
        }
    }

    /// <summary>
    /// Increase the health of a building smoothly over time
    /// </summary>
    /// <param name="currentHealth"></param>
    /// <param name="healthPerSecond">The increment of health per second</param>
    /// <returns></returns>
    private IEnumerator RepairBuildingCR(int currentHealth, int healthPerSecond)
    {
        while (health < MaxHealth && resources.AmountIt >= healthPerSecond)
        {
            health += healthPerSecond;
            healthCreator.ChangeHealth(health);

            resources.Decrease_IT(healthPerSecond);

            yield return new WaitForSeconds(1);
        }

        if (health > MaxHealth)
        {
            health = MaxHealth;
        }
    }

    [Command]
    public void CmdGetDamage(int damage)
    {
        if (!isLocalPlayer)
        {
            OnChangeHealth(health - damage);
            return;
        }

        health -= damage;
    }

    /// <summary>
    /// Change the health and maxHealth of the building to the newVal
    /// </summary>
    /// <param name="newVal">The new health value</param>
    public void ChangeHealth(int newVal)
    {
        MaxHealth = newVal;
        Health = MaxHealth;
    }

    /// <summary>
    /// Change the healthBar according to the amount of health
    /// </summary>
    /// <param name="health">Amount of health</param>
    public void OnChangeHealth(int health)
    {
        Health = health;

        if (Health <= 0)
        {
            SpawnDeathParticle(transform.position);
            if (gameObject.layer != 19)
            {
                IsDead = true;
                NetworkServer.Destroy(this.gameObject);
            }
            else
            {
                //Reset state of Outpost
                UnitInit outpostInit = GetComponent<UnitInit>();
                outpostInit.ChangeColor(true);
                OutpostBehaviour outpostBehaviour = GetComponent<OutpostBehaviour>();
                outpostBehaviour.RootNetId = null;
                gameObject.transform.SetParent(SceneCache.instance.water.transform);
            }
        }

        if (Health < maxHealth && !hasActivatedCanvas)
        {
            ActivateHealthBar(true);
            hasActivatedCanvas = true;
        }

        healthCreator.ChangeHealth(health);
    }

    public void SpawnDeathParticle(Vector3 location)
    {
        var dp = Instantiate(deathParticleEffect, location, Quaternion.identity);
        Destroy(dp, 5f);

        RpcSpawnDeathParticle(location);
    }

    [ClientRpc]
    public void RpcSpawnDeathParticle(Vector3 location)
    {
        var dp = Instantiate(deathParticleEffect, location, Quaternion.identity);
        Destroy(dp, 5f);
    }

    /// <summary>
    /// Explode and remove the building from Lists to avoid missing ref errors
    /// </summary>
    private void OnDestroy()
    {
        if (localPlayerNetId.isServer)
        {
            baseConnection.RemoveBuildingFromTrackList(this.gameObject);
        }

        if (hasAuthority)
        {
            unitSelectManager.RemoveUnit(this.gameObject);
        }
    }

    public int Health
    {
        get { return health; }
        set { health = value; }
    }

    public int MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    public bool IsDead { get; private set; }
}

//Uncomment for building health testing 
//public class MenuItems : MonoBehaviour
//{
//    // Add this method to the script in the inspector
//    // Does a tenth of the max health as damage
//    [MenuItem("CONTEXT/BuildingHealth/Do 10% Damage")]
//    static void DoTenPercentDamage(MenuCommand command)
//    {
//        BuildingHealth bh = (BuildingHealth)command.context;
//        bh.DoDamage((int)(bh.MaxHealth * 0.1f));
//    }

//    [MenuItem("CONTEXT/BuildingHealth/Do 25% Damage")]
//    static void DoTwentyFivePercentDamage(MenuCommand command)
//    {
//        BuildingHealth bh = (BuildingHealth)command.context;
//        bh.DoDamage((int)(bh.MaxHealth * 0.25f));
//    }

//    [MenuItem("CONTEXT/BuildingHealth/Do 50% Damage")]
//    static void DoFiftyPercentDamage(MenuCommand command)
//    {
//        BuildingHealth bh = (BuildingHealth)command.context;
//        bh.DoDamage((int)(bh.MaxHealth * 0.5f));
//    }
//}