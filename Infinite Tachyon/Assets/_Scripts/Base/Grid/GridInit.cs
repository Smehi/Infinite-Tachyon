﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GridInit : NetworkBehaviour
{
    public GameObject gridCell;

    // Instantiate the first cell for the grid
    void Start()
    {
        Instantiate(gridCell, this.transform);
    }
}
