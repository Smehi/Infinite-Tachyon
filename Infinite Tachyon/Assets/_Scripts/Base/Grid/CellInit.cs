﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Every cell creates other cells around it self to make a circle of cells
public class CellInit : MonoBehaviour
{

    private BoxCollider boxCollider;
    private SphereCollider inBoundsCollider;
    public GameObject newCell;
    private Vector3 cellDistance;
    public bool isPlaceable;
    private BaseConnection baseConnection;

    //Checks if there is an identical cell or if it is outside the range of the grid
    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        cellDistance = boxCollider.bounds.max - boxCollider.bounds.min;
        if (Vector3.Distance(transform.parent.position, transform.position) > 15 * 0.92f)
        {
            isPlaceable = false;            
        }
        else
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                if (transform.parent.GetChild(i) == null) break;
                if (transform.parent.GetChild(i) != this.transform && transform.parent.GetChild(i).transform.position == transform.position)
                {
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }

    // creates 4 cells around it self if its not on the edge of the grid
    void Start()
    {
        transform.name = "GridCell";
        if (isPlaceable)
        {
            Instantiate(newCell, transform.position + new Vector3(-cellDistance.x, 0, 0), Quaternion.identity, transform.parent);
            Instantiate(newCell, transform.position + new Vector3(cellDistance.x, 0, 0), Quaternion.identity, transform.parent);

            Instantiate(newCell, transform.position + new Vector3(0, 0, cellDistance.z), Quaternion.identity, transform.parent);
            Instantiate(newCell, transform.position + new Vector3(0, 0, -cellDistance.x), Quaternion.identity, transform.parent);
        }
        Invoke("GridExists", 0.5f);
    }

    void GridExists()
    {
        baseConnection = GetComponentInParent<BaseConnection>();
        baseConnection.CellsAlive = true;
    }
}
