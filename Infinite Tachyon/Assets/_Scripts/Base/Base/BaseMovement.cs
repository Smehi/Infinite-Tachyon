﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseMovement : MonoBehaviour
{
    private float radius;
    public float speed;

    private Vector3 baseStartPoint;
    private Vector3 destination;
    private Vector3 start;
    private float progress;

    private NavMeshAgent agent;
    private bool reached;
    private int collidedUnits;
    private CapsuleCollider baseCollider;

    void Start()
    {
        start = transform.position;
        baseStartPoint = transform.position;
        progress = 0.0f;
        agent = GetComponent<NavMeshAgent>();
        baseCollider = GetComponent<CapsuleCollider>();
        baseCollider.radius = 20f;
        radius = SceneCache.instance.water.GetComponentInChildren<SphereCollider>().radius * 0.8f;
        PickNewDestination();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (agent.isActiveAndEnabled)
        {
            //When the base is barely moving 
            if (agent.velocity.sqrMagnitude < 0.5f)
            {
                PickNewDestination();
            }

            //Check whether the base is close enough to its destination
            if (agent.remainingDistance <= 1.0f)
            {
                reached = true;
            }
            else
            {
                reached = false;
            }
        }
        else
        {
            //Move as a navMeshObstacle
            MoveWithoutAgent(destination);
        }

        if (reached)
        {
            start = destination;
            PickNewDestination();
            progress = 0.0f;
        }

        //Test the Randomness of Random.insideUnitSphere
        //Debug.DrawRay(new Vector3(Random.insideUnitSphere.x * radius, transform.position.y, Random.insideUnitSphere.z * radius), new Vector3(1,0,1), Color.green, 50f);
    }


    /// <summary>
    /// Give the base a new random destination 
    /// </summary>
    private void PickNewDestination()
    {
        if (agent.isActiveAndEnabled)
        {
            destination = new Vector3(Random.insideUnitCircle.x * radius, transform.position.y,
                Random.insideUnitCircle.y * radius);
            agent.SetDestination(destination);
        }
    }

    /// <summary>
    /// Show a line to see in the editor where the base is headed towards
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(transform.position, destination);
    }

    /// <summary>
    /// Move the base without the navMeshAgent, this is needed when the base is a navMeshObstacle
    /// </summary>
    /// <param name="dest"></param>
    private void MoveWithoutAgent(Vector3 dest)
    {
        transform.position = (Vector3.Lerp(transform.position, dest, speed * Time.fixedUnscaledDeltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other != this)
        {
            if (other.CompareTag("UnitAir") || other.CompareTag("UnitGround") || other.CompareTag("MainBase"))
            {
                collidedUnits++;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other != this)
        {
            if (other.CompareTag("UnitAir") || other.CompareTag("UnitGround") || other.CompareTag("MainBase"))
            {
                collidedUnits--;
                PickNewDestination();
            }
        }
    }
}