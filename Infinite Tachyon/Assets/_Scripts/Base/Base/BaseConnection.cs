﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

// manages all the networking for the base (spawning on the network for the base and buildings).
// keeps track of the amount of buildings.
public class BaseConnection : NetworkBehaviour
{
    // Use this for initialization
    [SerializeField] private GameObject basePrefab;
    [SerializeField] private GameObject Headquarter;
    [SerializeField] private GameObject airfield;
    [SerializeField] private GameObject docks;
    [SerializeField] private GameObject techlab;
    [SerializeField] private GameObject powerGenerator;

    [SerializeField] private List<Building> buildings;

    public List<GameObject> amountDocks;
    public List<GameObject> amountAirfields;
    public List<GameObject> amountHeadquarters; //Just for the UI and incase we'll have more??

    private GameObject baseObj;
    private GameObject Hq;
    private GameObject currentConstruction;

    private SpawnUnitsUI spawnUnitsUI;
    [SyncVar] public bool CellsAlive;
    private bool HqIsAlive;

    private List<GameObject> serverBuildings = new List<GameObject>();
    private List<GameObject> clientBuildings = new List<GameObject>();

    public List<Building> GetBuildings
    {
        get
        {
            return buildings;
        }

        set
        {
            buildings = value;
        }
    }

    private void Start()
    {
        Invoke("Spawn", 0.1f);
    }

    void Spawn()
    {
        if (GetComponent<NetworkIdentity>().hasAuthority)
        {
            spawnUnitsUI = LobbyManager.instance.localPlayer.transform.GetComponent<SpawnUnitsUI>();
        }
        else
        {
            spawnUnitsUI = LobbyManager.instance.notLocalPlayer.transform.GetComponent<SpawnUnitsUI>();
        }

        if (!isLocalPlayer)
        {
            GetComponent<BaseConnection>().enabled = false;
        }
        else
        {
            CmdSpawnBase();
        }
    }

    private void FixedUpdate()
    {
        if (spawnUnitsUI != null)
        {
            ActivateUI();
        }

        if (CellsAlive && !HqIsAlive && isLocalPlayer)
        {
            CmdSpawnHQ();
            HqIsAlive = true;
        }
    }

    [Command]
    void CmdSpawnBase()
    {
        baseObj = Instantiate(basePrefab, this.transform);
        NetworkServer.SpawnWithClientAuthority(baseObj, connectionToClient);
    }

    [Command]
    void CmdSpawnHQ()
    {
        Hq = Instantiate(Headquarter, new Vector3(transform.position.x, -.9f, transform.position.z), Quaternion.identity, this.transform);
        NetworkServer.SpawnWithClientAuthority(Hq, connectionToClient);
    }

    [Command]
    public void CmdSpawnBuilding(string buildingName, float x, float y, float z, bool isBuilding)
    {
        Building currentBuilding = SearchForBuilding(buildingName, buildings);
        currentConstruction = Instantiate(currentBuilding.prefab, new Vector3(x, y, z), Quaternion.identity, Hq.transform);
        BuildingInit buildingInit = currentConstruction.GetComponentInChildren<BuildingInit>();
        buildingInit.BuildTime = currentBuilding.cooldown.y;
        buildingInit.GetIT_required = currentBuilding.IT_required;
        buildingInit.EnergyRequired = currentBuilding.energyRequired;
        NetworkServer.SpawnWithClientAuthority(currentConstruction, connectionToClient);
    }

    // Gets the correct building
    public Building SearchForBuilding(string name, List<Building> buildings)
    {
        Building buildingObj = null;
        foreach (Building item in buildings)
        {
            if (item.name == name)
                buildingObj = item;
        }

        //Debug.Log(name);

        return buildingObj;
    }

    private void ActivateUI()
    {
        if (amountDocks.Count > 0)
        {
            if (OneSelected(amountDocks))
            {
                spawnUnitsUI.groundTab.SetActive(true);
                if (!spawnUnitsUI.airUI.activeSelf && !spawnUnitsUI.headquartersUI.activeSelf)
                {
                    spawnUnitsUI.groundUI.SetActive(true);
                }
            }
            else
            {
                spawnUnitsUI.groundTab.SetActive(false);
                spawnUnitsUI.groundUI.SetActive(false);
            }
        }

        if (amountAirfields.Count > 0)
        {
            if (OneSelected(amountAirfields))
            {
                spawnUnitsUI.airTab.SetActive(true);
                if (!spawnUnitsUI.groundUI.activeSelf && !spawnUnitsUI.headquartersUI.activeSelf)
                {
                    spawnUnitsUI.airUI.SetActive(true);
                }
            }
            else
            {
                spawnUnitsUI.airTab.SetActive(false);
                spawnUnitsUI.airUI.SetActive(false);
            }
        }

        if (amountHeadquarters.Count > 0)
        {
            if (OneSelected(amountHeadquarters))
            {
                spawnUnitsUI.headquartersTab.SetActive(true);
                if (!spawnUnitsUI.airUI.activeSelf && !spawnUnitsUI.groundUI.activeSelf)
                {
                    spawnUnitsUI.headquartersUI.SetActive(true);
                }
            }
            else
            {
                spawnUnitsUI.headquartersTab.SetActive(false);
                spawnUnitsUI.headquartersUI.SetActive(false);
            }
        }
    }

    // to switch UI tabs
    public void TabTapped(string name)
    {
        switch (name)
        {
            case "Air":
                {
                    spawnUnitsUI.airUI.SetActive(true);
                    spawnUnitsUI.groundUI.SetActive(false);
                    spawnUnitsUI.headquartersUI.SetActive(false);
                }
                break;
            case "Ground":
                {
                    spawnUnitsUI.groundUI.SetActive(true);
                    spawnUnitsUI.airUI.SetActive(false);
                    spawnUnitsUI.headquartersUI.SetActive(false);
                }
                break;
            case "Headquarters":
                {
                    spawnUnitsUI.headquartersUI.SetActive(true);
                    spawnUnitsUI.airUI.SetActive(false);
                    spawnUnitsUI.groundUI.SetActive(false);
                }
                break;
        }

    }

    //Checks whether atleast one of a list is selected
    private bool OneSelected(List<GameObject> objList)
    {
        bool OneSelect = false;
        int amountSelected = 0;

        for (int i = 0; i < objList.Count; i++)
        {
            var building = objList[i];
            if (building != null)
            {
                UnitSelect buildSelect = building.GetComponentInChildren<UnitSelect>();

                if (buildSelect.IsSelected)
                {
                    amountSelected++;
                    break;
                }
            }
            else
            {
                objList.Remove(building);
            }
        }

        if (amountSelected > 0)
        {
            OneSelect = true;
        }
        else
        {
            OneSelect = false;
        }

        return OneSelect;
    }

    public void AddBuildingToTrackList(GameObject building)
    {
        if (building.GetComponent<NetworkIdentity>().hasAuthority)
        {
            serverBuildings.Add(building);
        }
        else
        {
            clientBuildings.Add(building);
        }
    }

    public void RemoveBuildingFromTrackList(GameObject building)
    {
        if (building.GetComponent<NetworkIdentity>().hasAuthority)
        {
            serverBuildings.Remove(building);
        }
        else
        {
            clientBuildings.Remove(building);
        }

        CheckGameOver();
    }

    private void CheckGameOver()
    {
        if (serverBuildings.Count == 0 || clientBuildings.Count == 0)
        {
            RpcGameOver(serverBuildings.Count, clientBuildings.Count);
        }
    }

    [ClientRpc]
    private void RpcGameOver(int serverBuildingsCount, int clientBuildingsCount)
    {
        if (serverBuildingsCount == 0)
        {
            if (isServer)
            {
                ResultScreen rs = LobbyManager.instance.localPlayer.transform.Find("Main Camera").GetComponent<ResultScreen>();
                rs.HasWon = false;
            }
            else
            {
                ResultScreen rs = LobbyManager.instance.localPlayer.transform.Find("Main Camera").GetComponent<ResultScreen>();
                rs.HasWon = true;
            }
        }
        else if (clientBuildingsCount == 0)
        {
            if (isServer)
            {
                ResultScreen rs = LobbyManager.instance.localPlayer.transform.Find("Main Camera").GetComponent<ResultScreen>();
                rs.HasWon = true;
            }
            else
            {
                ResultScreen rs = LobbyManager.instance.localPlayer.transform.Find("Main Camera").GetComponent<ResultScreen>();
                rs.HasWon = false;
            }
        }
    }
}
[System.Serializable]
public class Building
{
    public GameObject prefab;
    public string name;
    public GameObject construction;
    public Image cooldownImage;
    public Vector2 cooldown;
    public float IT_required;
    public float energyRequired;
}