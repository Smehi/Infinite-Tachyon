﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BaseInit : NetworkBehaviour
{
    // Sets the correct parent for the base

    void Start()
    {
        Invoke("DelayedStart", .2f);
    }

    void DelayedStart() {

        NetworkIdentity netID = GetComponent<NetworkIdentity>();
        if (netID.hasAuthority)
        {
            this.transform.SetParent(LobbyManager.instance.localPlayer.transform);
        }
        else
        {
            this.transform.SetParent(LobbyManager.instance.notLocalPlayer.transform);
        }
    }
}
