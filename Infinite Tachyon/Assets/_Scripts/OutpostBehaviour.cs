﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class OutpostBehaviour : NetworkBehaviour
{
    public GameObject vision;

    [SyncVar] private Transform parent;

    private BuildingHealth buildingHealth;
    [SerializeField] private GameObject hitCollider;
    private NetworkIdentity networkIdentity;
    public UnitInit outPostInit;
    [SyncVar] private bool parentSet, capturedBefore;
    [SyncVar] private int amountUnits;
    [SyncVar] private NetworkIdentity rootNetId;
    [SyncVar(hook = "CaptureOutpost")] private GameObject captureUnit;
    private UnitCombat unitCombat;

    // Use this for initialization
    void Start()
    {
        networkIdentity = GetComponent<NetworkIdentity>();
        buildingHealth = GetComponent<BuildingHealth>();
        vision.SetActive(false);
        capturedBefore = false;
    }

    // Update is called once per frame
    void Update()
    {
        //if (parent != null && !parentSet)
        //{
        //    InitializeOutpost(parent);
        //}

        if (rootNetId == null)
            return;
        if (rootNetId.transform != transform.root)
        {
            //  outPostInit.enabled = true;
            print("Transform wrong");
            InitializeOutpost();
            // outPostInit.ChangeColor();
        }
    }

    public void InitializeOutpost()
    {
        if (rootNetId == null) return;

        vision.SetActive(rootNetId.isLocalPlayer);
        transform.SetParent(rootNetId.transform);

        outPostInit.enabled = true;

        outPostInit.ChangeColor(false);

        buildingHealth.ChangeHealth(buildingHealth.Health);

        if (capturedBefore)
        {
            buildingHealth.healthCreator.ResetHealthBar(buildingHealth.Health);
        }
        else
        {
            buildingHealth.healthCreator.InitializeHealth(buildingHealth.Health);
        }

        buildingHealth.ActivateHealthBar(true);

        if (buildingHealth.Health > 0)
        {
            //outPostInit.ChangeColor();
            capturedBefore = true;
            if (unitCombat != null)
            {
                unitCombat.CmdGetDamage(unitCombat.UnitHealth);
            }

            amountUnits = 0;
        }
    }

    public void CaptureOutpost(GameObject unit)
    {
        if (unit == null) return;
        if (buildingHealth.Health > 0) return;
        amountUnits++;
        if (amountUnits > 1)
            return;

        captureUnit = unit;

        unitCombat = unit.GetComponent<UnitCombat>();

        buildingHealth.Health = unitCombat.UnitHealth;

        outPostInit.enabled = true;

        transform.SetParent(unit.transform.root);
        RootNetId = unit.transform.root.GetComponent<NetworkIdentity>();
        InitializeOutpost();
    }


    //When health <= 0, allow it to be captured by any player
    //Who is close to the SphereCollider
    private void OnTriggerEnter(Collider other)
    {
        if (buildingHealth != null)
        {
            if (buildingHealth.Health <= 0)
            {
                if (other.gameObject.layer == 9)
                {
                    UnitCombat unitCombatScript = other.GetComponentInParent<UnitCombat>();
                    if (unitCombatScript.IsAttackMove)
                    {
                        var unitId = other.transform.parent.parent.GetComponent<NetworkIdentity>();
                        unitCombatScript.CmdCapture(gameObject, other.transform.parent.parent.gameObject);
                        //CaptureOutpost(other.gameObject);
                    }
                }
            }
        }
    }

    //public Transform Parent
    //{
    //    get { return parent; }
    //    set { parent = value; }
    //}

    public NetworkIdentity RootNetId
    {
        get
        {
            return rootNetId;
        }
        set
        {
            rootNetId = value;
            //if (rootNetId != null)
            //{
            //    InitializeOutpost();
            //}
        }
    }

    public GameObject CaptureUnit
    {
        get
        {
            return captureUnit;
        }
        set
        {
            captureUnit = value;
        }
    }
}