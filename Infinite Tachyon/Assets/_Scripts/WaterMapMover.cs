﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WaterMapMover : MonoBehaviour
{
    private Material material;
    private MaterialPropertyBlock materialPropertyBlock;
    [SerializeField] private float scrollSpeedX, scrollSpeedY;

    private void Start()
    {
        material = GetComponent<MeshRenderer>().material;
    }

    private void Update()
    {
        material.SetTextureOffset("_NormalMap",
            new Vector2(Mathf.Sin(Time.time * scrollSpeedX), Mathf.Sin(Time.time * scrollSpeedY)));
    }
}