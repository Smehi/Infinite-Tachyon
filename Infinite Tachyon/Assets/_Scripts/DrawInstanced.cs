﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawInstanced : MonoBehaviour
{
    const float BATCH_MAX_FLOAT = 1023f;
    const int BATCH_MAX = 1023;

    public GameObject[] instancePrefab;
    public Material[] instanceMaterial;

    [Tooltip("Amount of redraws")] public int cycles;
    private List<MeshFilter> meshFilter;
    private List<MeshRenderer> meshRenderer;

    private Matrix4x4[] matrices;

    [Tooltip("Some prefabs have the mesh in the children")]
    public bool meshInChildren;

    public Vector2 scale;
    public CrystalCluster[] crystalClusters;

    [SerializeField] private bool DrawOnGPU;

    // Use this for initialization
    void Start()
    {
        FillMatrix();
    }

    //Fill matrix with all positions, rotations and scales
    private void FillMatrix()
    {
        int count = crystalClusters.Length * cycles;

        matrices = new Matrix4x4[count];

        for (int i = 0; i < crystalClusters.Length; i++)
        {
            for (int j = 0; j < cycles; j++)
            {
                int index = i * cycles + j;

                matrices[index] = Matrix4x4.identity;

                Vector3 newScale = new Vector3(transform.localScale.x * scale.x, 1, transform.localScale.z * scale.y);
                CrystalCluster crystal = crystalClusters[i];
                Quaternion rotation = Quaternion.Euler(crystal.rotation);

                matrices[index].SetTRS(crystal.position, rotation, newScale);
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        int total = crystalClusters.Length * cycles;
        int batches = Mathf.CeilToInt(total / BATCH_MAX_FLOAT);

        for (int i = 0; i < batches; i++)
        {
            int batchCount = Mathf.Min(BATCH_MAX, total - (BATCH_MAX * i));
            int start = Mathf.Max(0, (i - 1) * BATCH_MAX);

            Matrix4x4[] batchedMatrices = GetBatchedMatrices(start, batchCount);

            foreach (GameObject prefab in instancePrefab)
            {
                MeshFilter meshFilt;
                Material material;
                if (meshInChildren)
                {
                    meshFilt = prefab.GetComponentInChildren<MeshFilter>();
                    material = prefab.GetComponentInChildren<MeshRenderer>().sharedMaterial;
                }
                else
                {
                    meshFilt = prefab.GetComponent<MeshFilter>();
                    material = prefab.GetComponent<MeshRenderer>().sharedMaterial;
                }

                Graphics.DrawMeshInstanced(meshFilt.sharedMesh, 0, material, batchedMatrices, batchCount);
            }
        }
    }


    private Matrix4x4[] GetBatchedMatrices(int offset, int batchCount)
    {
        Matrix4x4[] batchedMatrices = new Matrix4x4[batchCount];

        for (int i = 0; i < batchCount; i++)
        {
            batchedMatrices[i] = matrices[i + offset];
        }

        return batchedMatrices;
    }
}

[System.Serializable]
public class CrystalCluster
{
    public Vector3 position;
    public Vector3 rotation;
}