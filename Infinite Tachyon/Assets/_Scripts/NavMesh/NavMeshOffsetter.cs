﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMeshOffsetter : MonoBehaviour
{
    [SerializeField] private float offset;

    // Use this for initialization
    void Start()
    {
        transform.position += Vector3.up * offset;
    }
}
