﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WorkerBehaviour : MonoBehaviour
{
    private PlayerResources resManager;
    [SerializeField] private float stored_IT;
    [SerializeField] private int IT_per_second;
    [SerializeField] private int max_stored_IT;
    [SerializeField, Range(0, 1f)] private float waterCircle_multiplier;
    private float gather_interval;

    private Rigidbody body;
    private NavMeshAgent agent;
    private Vector3 savedDirection;
    private Vector3 offSet;
    private Renderer rend;

    private Vector3 savedIT_location, nullVector;
    [SerializeField] private GameObject main_base;
    private Vector3 main_base_position;
    private GameObject waterObject;
    public Vector3 velocity;
    private bool foundIT;

    private CapsuleCollider baseCollider;
    private SphereCollider waterCollider;
    private UnitAnimation unitAnimationScript;
    [SerializeField] private GameObject suckObject;
    private bool isCollecting, isEmptying;
    private bool coroutineActive;

    void Start()
    {
        if (main_base == null)
        {
            Transform rootTrans = transform.root;
            for (int i = 0; i < rootTrans.childCount; i++)
            {
                if (rootTrans.GetChild(i).CompareTag("MainBase"))
                {
                    main_base = rootTrans.GetChild(i).gameObject;
                }
            }
        }

        if (suckObject == null)
        {
            suckObject = transform.parent.Find("Sucking").gameObject;
        }

        unitAnimationScript = GetComponentInParent<UnitAnimation>();
        waterObject = SceneCache.instance.water;
        resManager = transform.root.GetComponent<PlayerResources>();
        body = GetComponentInParent<Rigidbody>();
        agent = GetComponentInParent<NavMeshAgent>();
        foundIT = false;
        main_base_position = new Vector3(main_base.transform.position.x, transform.position.y,
            main_base.transform.position.z);
        gather_interval = 1f;
        coroutineActive = false;
        isEmptying = false;
        isCollecting = false;
    }


    private void FixedUpdate()
    {
        if (!isCollecting && !IsIdle())
        {
            if (!IsWithinCircle(waterObject, waterCircle_multiplier))
            {//When the worker is close to the crystals start collecting
                isCollecting = true;
            }
        }

        if (isCollecting)
        {//When collecting stop moving entirely
            body.velocity = Vector3.zero;
            foundIT = true;
            agent.isStopped = true;

            if (gather_interval > 0f)
            {
                gather_interval -= Time.fixedDeltaTime;
            }

            GatherResources();
        }

        if (isEmptying)
        {
            if (gather_interval > 0f)
            {
                gather_interval -= Time.fixedDeltaTime;
            }

            EmptyResources();
        }

        if (stored_IT > 0f && !isCollecting)
        {
            ReturnToBase();

            if (!isEmptying && !IsIdle())
            {
                if (IsWithinCircle(main_base, 2f))
                {
                    isEmptying = true;
                }
            }
        }
    }

    /// <summary>
    /// Checks whether the worker is within a circle using the radius of a collider
    /// </summary>
    /// <param name="circle"></param>
    /// <param name="radiusMultiplier"></param>
    /// <returns></returns>
    bool IsWithinCircle(GameObject circle, float radiusMultiplier)
    {
        float distance = 0f;
        float radius = (circle.transform.localScale.z * radiusMultiplier) / 2;

        Vector2 circleCenterPosition = new Vector2(circle.transform.position.x, circle.transform.position.z);

        Vector2 unitPosition = new Vector2(transform.position.x, transform.position.z);
        distance = (unitPosition - circleCenterPosition).sqrMagnitude;

        if (circle.CompareTag("MainBase"))
        {
            if (baseCollider == null)
            {
                baseCollider = circle.GetComponent<CapsuleCollider>();
            }

            if (baseCollider != null)
            {
                radius = baseCollider.radius * radiusMultiplier;
            }
        }
        else
        {
            if (waterCollider == null)
            {
                waterCollider = circle.GetComponentInChildren<SphereCollider>();
            }

            if (waterCollider != null)
            {
                radius = waterCollider.radius * radiusMultiplier;
            }
        }

        radius *= radius;
        if (distance > radius) return false;
        else return true;
    }

    /// <summary>
    /// Check whether the worker is idle
    /// </summary>
    /// <returns>Idle state</returns>
    private bool IsIdle()
    {
        if (agent.velocity.magnitude > Mathf.Epsilon) return false;
        else
        {
            return true;
        }
    }

    void GatherResources()
    {
        isCollecting = (stored_IT < max_stored_IT);

        if (isCollecting)
        {
            if (!suckObject.activeInHierarchy)
            {
                suckObject.SetActive(true);
            }

            if (gather_interval <= 0f)
            {
                gather_interval = 1f;
                stored_IT += (IT_per_second);
                savedIT_location = transform.position;
            }
        }
    }

    void ReturnToBase()
    {
        isEmptying = false;
        agent.isStopped = false;
        foundIT = false;

        if (!agent.pathPending)
        {
            agent.SetDestination(main_base.transform.position);
            suckObject.SetActive(false);
        }
    }

    void EmptyResources()
    {
        isEmptying = (stored_IT > 0);

        if (isEmptying && gather_interval <= 0f)
        {
            if (stored_IT > 0)
            {
                stored_IT -= IT_per_second;
                resManager.AmountIt += IT_per_second;
                savedIT_location += offSet;
                agent.SetDestination(savedIT_location);
            }
            else
            {
                stored_IT = 0;
            }
        }
    }
}