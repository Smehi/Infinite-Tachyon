﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class UnitSelectManager : NetworkBehaviour
{
    public delegate void MoveUnits(Vector3 point, bool isAttackMove, GameObject target);
    public event MoveUnits onMove;

    private Camera cam;
    private float groundPoint;

    private List<GameObject> existingUnits = new List<GameObject>();
    private List<GameObject> existingBuildings = new List<GameObject>();

    private Dictionary<GameObject, UnitSelect> objSelectDic = new Dictionary<GameObject, UnitSelect>();

    [Header("Single click selection")]
    [SerializeField] private LayerMask selectionLayerMask;

    private List<GameObject> selectedUnits;

    [Header("Selection Box")]
    [SerializeField] private RectTransform selectionBoxImage;
    [SerializeField] private float dragSelectSafezone;

    private Vector3 startPositionGui, endPositionGui;

    private List<GameObject> selectableUnits;
    private Vector3 mousePositionStart, mousePositionEnd;
    private Plane[] frustumPlanes;

    [Header("Unit Movement")]
    [SerializeField] private LayerMask unitAttackMoveLayerMask;
    [SerializeField] private LayerMask unitMoveLayerMask;
    [SerializeField] private GameObject moveClickEffect;
    [SerializeField] private GameObject attackMoveClickEffect;

    [Header("Formation")]
    [SerializeField] private float horizontalSpread;
    [SerializeField] private float verticalSpread;

    private SpawnUnitsUI unitSpawnCanvas;

    private void Start()
    {
        Invoke("DelayedStart", .2f);
    }

    void DelayedStart()
    {
        if (!isLocalPlayer)
        {
            this.enabled = false;
            return;
        }

        cam = GetComponentInChildren<Camera>();
        cam.transform.rotation = Quaternion.Euler(new Vector3(SceneCache.instance.cameraRotation.x, SceneCache.instance.cameraRotation.y , SceneCache.instance.cameraRotation.z));
        selectedUnits = new List<GameObject>();
        selectableUnits = new List<GameObject>();

        // Do a ray at the start of the game to get the y position on the ground collider
        Ray ray = cam.ScreenPointToRay(Vector3.zero);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, unitMoveLayerMask))
        {
            groundPoint = hit.point.y;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        #region Single/Multi select single click
        if (Input.GetMouseButtonDown(0))
        {
            // Save the start position of the mouse for the drag select box
            mousePositionStart = cam.ScreenToViewportPoint(Input.mousePosition);

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // Clear the selection if 'CTRL' is not hold and the mouse if not over a UI element
            // If the mouse if over a UI element but 'CTRL' is not being held down it will still not clear
            if (Input.GetAxisRaw("Multi Select") <= 0 && !IsPointerOverUIObject())
            {
                ClearSelection();
            }

            // Shoot the ray but only target object on the given layermask
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, selectionLayerMask))
            {
                // Try to get the value (UnitSelect) from the dictionary which hold all units with their corresponding UnitSelect
                // We have to use TryGetValue here because we can also click on enemy units which gives us an error
                // In other selection method this is not needed because we select from a List that is stored in this script
                UnitSelect unitSelectScript;
                objSelectDic.TryGetValue(hit.collider.transform.parent.parent.gameObject, out unitSelectScript);

                if (unitSelectScript != null && !unitSelectScript.IsSelected)
                {
                    if (unitSelectScript.parentNetworkId.hasAuthority)
                    {
                        selectedUnits.Add(hit.collider.transform.parent.parent.gameObject);
                        unitSelectScript.SelectUnit();
                    }
                }
            }
        }
        #endregion  

        #region Selection Box
        if (Input.GetMouseButton(0))
        {
            if (IsPointerOverUIObject())
                return;

            // Activate the selection box
            if (!selectionBoxImage.gameObject.activeSelf)
            {
                selectionBoxImage.gameObject.SetActive(true);
                startPositionGui = Input.mousePosition;
            }

            // Get the new end position of the box every frame
            endPositionGui = Input.mousePosition;

            Vector3 selectionBoxStart = startPositionGui;
            selectionBoxStart.z = 0;

            Vector3 selectionBoxCentre = (selectionBoxStart + endPositionGui) / 2;

            float sizeX = Mathf.Abs(selectionBoxStart.x - endPositionGui.x);
            float sizeY = Mathf.Abs(selectionBoxStart.y - endPositionGui.y);

            selectionBoxImage.sizeDelta = new Vector2(sizeX, sizeY);
            selectionBoxImage.position = selectionBoxCentre;
        }

        if (Input.GetMouseButtonUp(0))
        {
            selectionBoxImage.gameObject.SetActive(false);

            mousePositionEnd = cam.ScreenToViewportPoint(Input.mousePosition);

            // We want a little safezone so we don't cancel our selection if we move a bit when holding down LMB
            if (Vector3.Distance(mousePositionStart, mousePositionEnd) > dragSelectSafezone)
            {
                SelectUnitsInBox(mousePositionStart.x, mousePositionStart.y, mousePositionEnd.x - mousePositionStart.x, mousePositionEnd.y - mousePositionStart.y);
            }
        }
        #endregion

        #region Select all fighter units
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SelectAllFighterUnits(false);
        }
        #endregion

        #region Select all fighter units in screen
        if (Input.GetKeyDown(KeyCode.X))
        {
            SelectAllFighterUnits(true);
        }
        #endregion

        #region Move Units
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // If we hit a unit or a building with our move click
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, unitAttackMoveLayerMask) && !hit.transform.GetComponentInParent<NetworkIdentity>().hasAuthority)
            {
                // Check if we don't have any null units
                if (ClearEmpty())
                {
                    onMove?.Invoke(hit.point, true, hit.collider.gameObject);

                    GameObject effect = Instantiate(attackMoveClickEffect, new Vector3(hit.point.x, groundPoint, hit.point.z), Quaternion.Euler(-(hit.normal + (Vector3.right * 90))));
                    Destroy(effect, 1.25f);
                }
                else
                {
                    CancelInvoke();
                }
            }
            // If we do a normal move (we don't hit a unit or building or outpost)
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, unitMoveLayerMask))
            {
                // If our selection is bigger than 1 we want to make a formation
                if (selectedUnits.Count > 1 && ClearEmpty())
                {
                    List<Vector3> positions = UnitFormationMaker.MakeFormation(hit.point, horizontalSpread, verticalSpread, selectedUnits.Count);

                    for (int i = 0; i < selectedUnits.Count; i++)
                    {
                        objSelectDic[selectedUnits[i]].MoveInFormation(positions[i]);
                    }

                    GameObject effect = Instantiate(moveClickEffect, hit.point, Quaternion.Euler(-(hit.normal + (Vector3.right * 90))));
                    Destroy(effect, 1.25f);
                }
                // If we only have 1 unit we want to do the onMove event
                else if (selectedUnits.Count == 1)
                {
                    if (ClearEmpty())
                    {
                        onMove?.Invoke(hit.point, false, null);

                        GameObject effect = Instantiate(moveClickEffect, hit.point, Quaternion.Euler(-(hit.normal + (Vector3.right * 90))));
                        Destroy(effect, 1.25f);
                    }
                    else
                    {
                        CancelInvoke();
                    }
                }

            }
        }
        #endregion
    }

    private void OnDisable()
    {
        selectionBoxImage.gameObject.SetActive(false);
        startPositionGui = Vector3.zero;
        endPositionGui = Vector3.zero;
    }

    public void SetInExistingList(GameObject obj, UnitSelect unitSelect, ObjectType.Type type)
    {
        // Set the units in the right list and reference the UnitSelect
        switch (type)
        {
            case ObjectType.Type.Worker:
                break;
            case ObjectType.Type.Fighter:
                existingUnits.Add(obj);
                break;
            case ObjectType.Type.Building:
                existingBuildings.Add(obj);
                break;
        }

        objSelectDic[obj] = unitSelect;
        selectableUnits.Add(obj);
    }

    // Search selectableUnits and selectedUnits to see if there are empty object in there
    public bool ClearEmpty()
    {
        bool allowed = true;

        List<GameObject> removeUnits = new List<GameObject>();

        for (int i = 0; i < selectableUnits.Count; i++)
        {
            if (selectableUnits[i] == null)
            {
                removeUnits.Add(selectableUnits[i]);
            }
        }

        for (int i = 0; i < selectedUnits.Count; i++)
        {
            if (selectedUnits[i] == null)
            {
                removeUnits.Add(selectedUnits[i]);
            }
        }

        if (removeUnits.Count > 0)
        {
            allowed = false;

            foreach (var unit in removeUnits)
            {
                RemoveUnit(unit);
                Destroy(unit);
            }

            removeUnits.Clear();
            ClearSelection();
        }

        return allowed;
    }

    // Check every list that is in this script and remove the obj if it is in there
    public void RemoveUnit(GameObject obj)
    {
        if (selectedUnits.Contains(obj))
        {
            selectedUnits.Remove(obj);
        }

        if (selectableUnits.Contains(obj))
        {
            selectableUnits.Remove(obj);
        }

        if (existingUnits.Contains(obj))
        {
            existingUnits.Remove(obj);
        }

        if (existingBuildings.Contains(obj))
        {
            existingBuildings.Remove(obj);
        }

        if (objSelectDic.ContainsKey(obj))
        {
            objSelectDic.Remove(obj);
        }
    }

    private void SelectAllFighterUnits(bool inScreen)
    {
        List<GameObject> removeUnits = new List<GameObject>();

        if (Input.GetAxisRaw("Multi Select") <= 0 && !IsPointerOverUIObject())
        {
            ClearSelection();
        }

        if (inScreen)
        {
            // Get the 6 planes that represent the camera view
            frustumPlanes = GeometryUtility.CalculateFrustumPlanes(cam);

            // For each unit in the existing units get their bounds and chcek that against the planes
            foreach (var unit in ExistingUnits)
            {
                Bounds unitBounds = unit.transform.GetChild(0).Find("Model").GetComponentInChildren<Renderer>().bounds;

                if (unitBounds != null)
                {
                    if (GeometryUtility.TestPlanesAABB(frustumPlanes, unitBounds))
                    {
                        UnitSelect unitSelectScript = objSelectDic[unit];

                        if (!unitSelectScript.IsSelected && unitSelectScript.parentNetworkId.hasAuthority)
                        {
                            selectedUnits.Add(unit);
                            unitSelectScript.SelectUnit();
                        }
                    }
                }
                else
                {
                    removeUnits.Add(unit);
                }
            }
        }
        else
        {
            // Select all units that are available 
            foreach (var unit in ExistingUnits)
            {
                if (unit != null)
                {
                    UnitSelect unitSelectScript = objSelectDic[unit];

                    if (!unitSelectScript.IsSelected && unitSelectScript.parentNetworkId.hasAuthority)
                    {
                        selectedUnits.Add(unit);
                        unitSelectScript.SelectUnit();
                    }
                }
                else
                {
                    removeUnits.Add(unit);
                }
            }
        }

        if (removeUnits.Count > 0)
        {
            foreach (var unit in removeUnits)
            {
                RemoveUnit(unit);
            }

            removeUnits.Clear();
        }
    }

    private void SelectUnitsInBox(float startX, float startY, float endX, float endY)
    {
        List<GameObject> removeUnits = new List<GameObject>();

        if (Input.GetAxisRaw("Multi Select") <= 0 && !IsPointerOverUIObject())
        {
            ClearSelection();
        }

        // Make the rectangle which is the same size as the box that is shown on the UI
        Rect selectionRectangle = new Rect(startX, startY, endX, endY);

        foreach (var unit in selectableUnits)
        {
            if (unit != null)
            {
                // If the units in viewport coordinated is contained inside the box select that unit
                if (selectionRectangle.Contains(cam.WorldToViewportPoint(unit.transform.position), true))
                {
                    UnitSelect unitSelectScript = objSelectDic[unit];

                    if (!unitSelectScript.IsSelected && unitSelectScript.parentNetworkId.hasAuthority)
                    {
                        selectedUnits.Add(unit);
                        unitSelectScript.SelectUnit();
                    }
                }
            }
            else
            {
                removeUnits.Add(unit);
            }
        }

        if (removeUnits.Count > 0)
        {
            foreach (var unit in removeUnits)
            {
                RemoveUnit(unit);
            }

            removeUnits.Clear();
        }
    }

    public void ClearSelection()
    {
        // Run the 'DeselectUnit' method for every unit that is in the selected units list
        for (int i = 0; i < selectedUnits.Count; i++)
        {
            GameObject unit = selectedUnits[i];

            if (unit == null)
            {
                RemoveUnit(unit);
            }
            else
            {
                UnitSelect unitSelectScript = objSelectDic[unit];
                unitSelectScript.DeselectUnit();
            }
        }

        selectedUnits.Clear();
    }

    // When mouse if over an UI element
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    private void ActivateUI()
    {
        if (selectedUnits.Count > 0)
        {
            foreach (GameObject building in SelectedUnits)
            {
                if (building.layer != 12)
                    return;

                BuildingUI buildingUI = building.GetComponent<BuildingUI>();
                buildingUI.SpawnUI.SetActive(true);
            }
        }
    }

    public List<GameObject> ExistingUnits
    {
        get { return existingUnits; }
    }

    public List<GameObject> ExistingBuildings
    {
        get { return existingBuildings; }
    }

    public List<GameObject> SelectedUnits
    {
        get { return selectedUnits; }
        set { selectedUnits = value; }
    }
}
