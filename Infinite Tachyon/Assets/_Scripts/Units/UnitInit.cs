﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class UnitInit : NetworkBehaviour
{
    [SyncVar] private GameObject parent;
    [ColorUsage(true, true)] public Color newColor;
    private Color currentColor;
    private List<GameObject> playerColorObjects;

    private AudioManager audioManager;
    private UnitCombat unitCombat;
    public AudioSource audioSource;
    public AudioSource audioSourceEngine;

    public List<GameObject> publicColorObjects;

    private bool colorObtained = false;
    public GameObject visionObj;

    private WaitForSeconds invokeSeconds;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (playerColorObjects == null)
        {
            playerColorObjects = ColorList(this.gameObject);
        }

        invokeSeconds = new WaitForSeconds(.2f);

        if (CompareTag("Outpost"))
        {
            GetComponent<OutpostBehaviour>().outPostInit = this;
            enabled = false;
            return;
        }
    }

    public override void OnStartAuthority()
    {
        if (CompareTag("Outpost")) return;

        base.OnStartAuthority();

        if (visionObj != null)
        {
            visionObj.SetActive(true);
        }
        else
        {
            visionObj = transform.GetChild(0).Find("UnitVision").gameObject;
            visionObj.SetActive(true);
        }
    }

    private void Start()
    {
        if (parent != null)
        {
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                if (parent.transform.GetChild(i).name == "Units" && gameObject.layer == 18)
                {
                    this.transform.SetParent(parent.transform.GetChild(i));
                }

                if (parent.transform.GetChild(i).name == "Base(Clone)")
                {
                    this.transform.position = parent.transform.GetChild(i).position + new Vector3(20, 0, 0);
                }

                if (parent.transform.GetChild(i).name == "Outposts" && gameObject.layer == 19)
                {
                    this.transform.SetParent(parent.transform.GetChild(i));
                }
            }
        }

        StartCoroutine(SetColor());

        audioSource = GetComponent<AudioSource>();
        audioSourceEngine = GetComponent<AudioSource>();
        unitCombat = GetComponent<UnitCombat>();

        PlaySounds();
    }

    public void PlaySounds()
    {
        if (audioManager == null)
        {
            audioManager = FindObjectOfType<AudioManager>();
        }

        if (audioSource == null)
        {
            audioSource = this.gameObject.AddComponent<AudioSource>();
        }

        if (audioSourceEngine == null)
        {
            audioSourceEngine = this.gameObject.AddComponent<AudioSource>();
        }

        if (GetComponent<BuildingHealth>() == null)
        {
            audioManager.PlaySound(unitCombat.UnitTypeProperty, AudioManager.SoundType.Build, audioSource);
            audioManager.PlaySound(unitCombat.UnitTypeProperty, AudioManager.SoundType.Engine, audioSourceEngine);
        }
    }

    private IEnumerator SetColor()
    {
        yield return invokeSeconds;
        ChangeColor(false);
    }

    /// <summary>
    /// Changes the color of the units and buildings to the player color
    /// </summary>
    public void ChangeColor(bool returnState)
    {
        NetworkIdentity rootNetId = transform.root.GetComponent<NetworkIdentity>();
        if (rootNetId != null)
        {
            if (rootNetId.isLocalPlayer)
            {
                newColor = LobbyManager.instance.colors[LobbyManager.instance.playerColor];
                colorObtained = true;
            }
            else
            {
                newColor = LobbyManager.instance.colors[LobbyManager.instance.enemyColor];
                colorObtained = true;
            }
        }

        MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
        int ColorID = Shader.PropertyToID("_EmissionColor");
        if (returnState)
        {
            propBlock.SetVector(ColorID, Color.black);
        }
        else
        {
            propBlock.SetVector(ColorID, newColor);
        }

        foreach (GameObject colorObj in playerColorObjects)
        {
            if (colorObj.CompareTag("Trail"))
            {
                colorObj.GetComponent<TrailRenderer>().SetPropertyBlock(propBlock);
            }
            else
            {
                colorObj.GetComponent<MeshRenderer>().SetPropertyBlock(propBlock);
            }
        }
    }

    private List<GameObject> ColorList(GameObject obj)
    {
        List<GameObject> colorObjects = new List<GameObject>();

        if (publicColorObjects != null)
        {
            foreach (GameObject colorObj in publicColorObjects)
            {
                colorObjects.Add(colorObj);
            }
        }

        return colorObjects;
    }

    public GameObject Parent
    {
        get
        {
            return parent;
        }
        set
        {
            parent = value;
        }
    }

    public Color NewColor
    {
        get
        {
            return newColor;
        }

        set
        {
            newColor = value;
        }
    }
}