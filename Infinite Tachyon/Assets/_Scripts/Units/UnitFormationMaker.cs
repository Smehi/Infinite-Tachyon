﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitFormationMaker
{
    /// <summary>
    /// Makes a square formation around the click point, favoring width over depth.
    /// </summary>
    /// <param name="point">The point that the mouse hits the ground.</param>
    /// <param name="spreadHorizontal">The amount of spread on the X-axis.</param>
    /// <param name="spreadVertical">The amount of spread on the Z-axis.</param>
    /// <param name="selectionSize">The size of the units that are going to be in the formation.</param>
    /// <returns>Returns an array of positions that is made from the parameters given.</returns>
    public static List<Vector3> MakeFormation(Vector3 point, float spreadHorizontal, float spreadVertical, int selectionSize)
    {
        List<Vector3> positions = new List<Vector3>();

        float startX = new float();
        float startZ = new float();
        int unitsPerRow = Mathf.CeilToInt(Mathf.Sqrt(selectionSize));
        int depth = Mathf.CeilToInt((float)selectionSize / (float)unitsPerRow);

        // Get the middle point on the Z axis
        switch (depth % 2)
        {
            // Mouse point is between 2 points.
            // The first point is half of the (depth * spread) over plus another half spread because the mouse is between two points.
            case 0:
                startZ = point.z - (((depth * spreadVertical) / 2) - (spreadVertical / 2));
                break;
            // Mouse point is exactly on a point.
            // The first point is half of the (depth * spread) over
            case 1:
                startZ = point.z - (spreadVertical * (depth / 2));
                break;
        }

        switch (unitsPerRow % 2)
        {
            // Same as depth
            case 0:
                startX = point.x - (((unitsPerRow * spreadHorizontal) / 2) - (spreadHorizontal / 2));

                // Loop through the depth and rows and make the positions
                for (float z = 0; z < depth; z++)
                {
                    for (float x = 0; x < unitsPerRow; x++)
                    {
                        // Add the (x * spread) and (z * spread) to their start positions
                        positions.Add(new Vector3((x * spreadHorizontal) + startX, point.y, (z * spreadVertical) + startZ));
                    }
                }
                break;
            case 1:
                startX = point.x - (spreadHorizontal * ((unitsPerRow - 1) / 2));

                for (float z = 0; z < depth; z++)
                {
                    for (float x = 0; x < unitsPerRow; x++)
                    {
                        positions.Add(new Vector3((x * spreadHorizontal) + startX, point.y, (z * spreadVertical) + startZ));
                    }
                }
                break;
        }

        return positions;
    }
}
