﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class UnitCombat : NetworkBehaviour
{
    [Header("Unit properties")]
    [SerializeField] private UnitType unitType;
    public enum UnitType
    {
        Worker,
        GroundAntiGround,
        GroundAntiAir,
        GroundAntiGroundAir,
        AirAntiGround,
        AirAntiAir,
        AirAntiGroundAir
    }

    [SyncVar(hook = "OnChangeHealth")] [SerializeField] private int unitHealth;
    [Tooltip("This is the radius of the circle for the range collider.")]
    [SyncVar] [SerializeField] private int unitAttackRange;
    [SyncVar] [SerializeField] private int unitAttackDamage;
    [Tooltip("The speed of the projectile that this unit shoots.")]
    [Range(1, 100)] [SerializeField] private int unitProjectileSpeed;
    [Tooltip("The amount of times this unit shoots per second.")]
    [Range(.2f, 5f)] [SerializeField] private float unitAttackRate;
    [Range(5, 30)] [SerializeField] private float unitMaxSpeed;
    [Range(1, 10)] [SerializeField] private float unitAcceleration;

    [SerializeField] private GameObject deathParticleEffect;

    [SyncVar] private bool isAttackMove;
    private SphereCollider rangeCollider;
    private NavMeshAgent agent;
    private UnitSelectManager unitSelectManager;
    private UnitSelect unitSelect;

    [Header("Shooting")]
    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform[] projectilePositions;

    private Transform projectileParent;
    private bool enemyInSight = false;
    private bool canShoot = true;
    private float nextShot;
    private float nextShotReset;

    private List<GameObject> enemies = new List<GameObject>();
    private GameObject targetProjectileHolder;
    private int enemiesKilled;
    private int lastKilledEnemyID;

    [Header("Unit UI")]
    [SyncVar] private int unitMaxHealth;

    private bool hasActivatedCanvas = false;
    private HealthCreator healthCreator;
    private AudioManager audioManager;
    private UnitInit unitInit;
    private UnitAnimation unitAnimation;

    public OutpostBehaviour outpostBehaviour;
    [SyncVar] private NetworkIdentity rootNetId;

    [Header("Unit Upgrades")]
    [SerializeField] private Transform unitVision;
    [Range(1, 2)] [SerializeField] private float healthBoostPercentage;
    [Range(1, 2)] [SerializeField] private float attackRangeBoostPercentage;
    [Range(1, 2)] [SerializeField] private float attackDamageBoostPercentage;
    [Range(0, 1)] [SerializeField] private float hybridBoostPercentage;

    [SyncVar(hook = "OnUpgradeSelected")] private bool upgradeSelected = false;
    private UpgradeType upgradeType;
    public enum UpgradeType
    {
        None,
        HealthUpgrade,
        RangeUpgrade,
        DamageUpgrade,
        HybridUpgrade
    }

    [Header("Other")]
    [SerializeField] private float groundUnitOffset;
    [SerializeField] private float airUnitOffset;
    [SerializeField] private float airNavMeshOffset;

    private void Init()
    {
        unitMaxHealth = unitHealth;

        healthCreator = transform.GetChild(0).GetComponentInChildren<HealthCreator>();
        healthCreator.InitializeHealth(UnitMaxHealth);

        unitSelectManager = LobbyManager.instance.localPlayer.GetComponent<UnitSelectManager>();
        unitSelect = transform.GetChild(0).GetComponentInChildren<UnitSelect>();

        agent = GetComponentInParent<NavMeshAgent>();
        agent.speed = unitMaxSpeed;
        agent.acceleration = unitAcceleration;

        unitAnimation = GetComponent<UnitAnimation>();
        audioManager = FindObjectOfType<AudioManager>();
        unitInit = GetComponent<UnitInit>();

        projectileParent = LobbyManager.instance.localPlayer.transform.Find("Projectiles");
        nextShotReset = 1 / UnitAttackRate;
        nextShot = nextShotReset;

        unitVision.localScale *= attackRangeBoostPercentage;

        // Get the unit type of the current unit and add the corresponding collider(s) that will determine the range
        switch (unitType)
        {
            case UnitType.Worker:
                break;
            case UnitType.GroundAntiGround:
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, agent.baseOffset, 0);
                rangeCollider.radius = unitAttackRange;
                break;
            case UnitType.GroundAntiAir:
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, airUnitOffset - groundUnitOffset, 0);
                rangeCollider.radius = unitAttackRange;
                break;
            case UnitType.GroundAntiGroundAir:
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, agent.baseOffset, 0);
                rangeCollider.radius = unitAttackRange;
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, airUnitOffset - groundUnitOffset, 0);
                rangeCollider.radius = unitAttackRange;
                break;
            case UnitType.AirAntiGround:
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, -groundUnitOffset + -airNavMeshOffset, 0);
                rangeCollider.radius = unitAttackRange;
                break;
            case UnitType.AirAntiAir:
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, groundUnitOffset + airNavMeshOffset, 0);
                rangeCollider.radius = unitAttackRange;
                break;
            case UnitType.AirAntiGroundAir:
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, -groundUnitOffset + -airNavMeshOffset, 0);
                rangeCollider.radius = unitAttackRange;
                rangeCollider = gameObject.AddComponent<SphereCollider>();
                rangeCollider.isTrigger = true;
                rangeCollider.center = new Vector3(0, groundUnitOffset + airNavMeshOffset, 0);
                rangeCollider.radius = unitAttackRange;
                break;
        }
    }

    private void SetUpgrades()
    {
        switch (UpgradeTypeProperty)
        {
            case UpgradeType.None:
                break;
            case UpgradeType.HealthUpgrade:
                unitHealth = Mathf.CeilToInt(unitHealth * healthBoostPercentage);
                break;
            case UpgradeType.RangeUpgrade:
                unitAttackRange = Mathf.CeilToInt(unitAttackRange * attackRangeBoostPercentage);
                break;
            case UpgradeType.DamageUpgrade:
                unitAttackDamage = Mathf.CeilToInt(unitAttackDamage * attackDamageBoostPercentage);
                break;
            case UpgradeType.HybridUpgrade:
                unitHealth = Mathf.CeilToInt(unitHealth * (healthBoostPercentage * hybridBoostPercentage));
                unitAttackRange = Mathf.CeilToInt(unitAttackRange * (attackRangeBoostPercentage * hybridBoostPercentage));
                unitAttackDamage = Mathf.CeilToInt(unitAttackDamage * (attackDamageBoostPercentage * hybridBoostPercentage));
                break;
        }

        upgradeSelected = true;
    }

    private void FixedUpdate()
    {
        // We only want to go into the Fixed Update if this unit is not a worker.
        if (unitType != UnitType.Worker)
        {
            // Stop the unit when they are in range of the target (used for AttackMove commands)
            if (hasAuthority)
            {
                if (EnemyTarget != null && isAttackMove)
                {
                    if (!EnemyTarget.CompareTag("Outpost"))
                    {
                        if ((this.transform.position - EnemyTarget.transform.position).sqrMagnitude < UnitAttackRange * UnitAttackRange)
                        {
                            agent.SetDestination(transform.position);
                        }
                    }
                }
            }

            if (enemies.Count > 0)
            {
                if (enemies[0] == null)
                {
                    ClearEmptyEnemies();
                }
                else if (!enemyInSight)
                {
                    enemyInSight = true;
                    EnemyTarget = enemies[0];
                }
            }
            else if (enemies.Count == 0)
            {
                enemyInSight = false;
                EnemyTarget = null;
            }

            if (nextShot > 0)
            {
                nextShot -= Time.deltaTime;

                if (nextShot <= 0)
                {
                    canShoot = true;
                }
            }

            if (canShoot && enemyInSight)
                Shoot();
        }
    }

    private void Shoot()
    {
        if (!EnemyTarget)
            return;

        canShoot = false;
        nextShot = nextShotReset;

        // Try to find the projectile holder that corresponds with this enemy
        if (EnemyTarget)
        {
            var target = projectileParent.Find(EnemyTarget.transform.GetInstanceID().ToString());

            if (target)
                targetProjectileHolder = target.gameObject;
        }

        // If we cannot find the holder or the last one is not the right one we will make a new one
        if (!targetProjectileHolder || targetProjectileHolder.name != EnemyTarget.transform.GetInstanceID().ToString())
        {
            targetProjectileHolder = new GameObject();
            targetProjectileHolder.transform.SetParent(projectileParent);

            if (EnemyTarget)
            {
                targetProjectileHolder.name = EnemyTarget.transform.GetInstanceID().ToString();
            }
            else
            {
                Destroy(targetProjectileHolder);
                return;
            }
        }

        SpawnBullet(targetProjectileHolder);
    }

    private void SpawnBullet(GameObject holder)
    {
        if (holder)
        {
            if (unitAnimation != null)
                unitAnimation.IsAttacking = true;

            // Find a random position for the projectile to spawn from and set the information for that projectile
            var rndIdx = UnityEngine.Random.Range(0, projectilePositions.Length);
            var obj = Instantiate(projectile, projectilePositions[rndIdx].position, projectile.transform.rotation, holder.transform);

            var projectileBehaviour = obj.GetComponent<ProjectileBehaviour>();
            projectileBehaviour.Instantiator = gameObject;
            projectileBehaviour.UnitCombat = this;
            projectileBehaviour.EnemyTarget = EnemyTarget;
        }
        else
        {
            unitAnimation.IsAttacking = false;
        }

        audioManager.PlaySound(UnitTypeProperty, AudioManager.SoundType.Shoot, unitInit.audioSource);
    }

    public void ClearEmptyEnemies()
    {
        enemyInSight = false;

        for (var i = 0; i < enemies.Count; i++)
            if (enemies[i] == null)
                enemies.RemoveAt(i);
    }

    // This is used to set the authority for the outpost that is captured by this unit
    [Command]
    public void CmdSetAuth(NetworkInstanceId objectId, NetworkIdentity player)
    {
        var iObject = NetworkServer.FindLocalObject(objectId);
        var networkIdentity = iObject.GetComponent<NetworkIdentity>();
        var otherOwner = networkIdentity.clientAuthorityOwner;

        if (otherOwner == player.connectionToClient)
        {
            return;
        }
        else
        {
            if (otherOwner != null)
            {
                networkIdentity.RemoveClientAuthority(otherOwner);
            }

            networkIdentity.AssignClientAuthority(player.connectionToClient);
        }
    }

    [Command]
    public void CmdRemoveAuth(NetworkIdentity netwId, NetworkIdentity playerId)
    {
        netwId.RemoveClientAuthority(playerId.connectionToClient);
    }

    // Activate the healthbar with a state which works out if it should be on or off
    public void ActivateHealthBar(bool state)
    {
        if (!state)
        {
            if (unitHealth == unitMaxHealth)
            {
                healthCreator.gameObject.SetActive(state);
                hasActivatedCanvas = false;
            }
        }
        else
        {
            healthCreator.gameObject.SetActive(state);
        }
    }

    // This command is used for when damage is done to units
    [Command]
    public void CmdGetDamage(int damage)
    {
        if (!isLocalPlayer)
        {
            OnChangeHealth(unitHealth - damage);
            return;
        }

        unitHealth -= damage;
    }

    [Command]
    public void CmdCapture(GameObject captureTarget, GameObject unit)
    {
        OutpostBehaviour target = captureTarget.GetComponent<OutpostBehaviour>();
        if (!isLocalPlayer)
        {
            target.CaptureOutpost(unit);
            return;
        }

        target.CaptureUnit = unit;
    }

    public void SpawnDeathParticle(Vector3 location)
    {
        var dp = Instantiate(deathParticleEffect, location, Quaternion.identity);
        Destroy(dp, 5f);

        RpcSpawnDeathParticle(location);
    }

    [ClientRpc]
    public void RpcSpawnDeathParticle(Vector3 location)
    {
        var dp = Instantiate(deathParticleEffect, location, Quaternion.identity);
        Destroy(dp, 5f);
    }

    private void OnChangeHealth(int health)
    {
        UnitHealth = health;
        healthCreator.ChangeHealth(health);

        if (unitHealth <= 0)
        {
            SpawnDeathParticle(transform.position);
            IsDead = true;
            audioManager.StartCoroutine(audioManager.Play("UnitDeath", unitInit.audioSource));
            GetComponent<NetworkTransform>().sendInterval = 0;
            NetworkServer.Destroy(gameObject);
        }

        if (unitHealth < unitMaxHealth && !hasActivatedCanvas)
        {
            ActivateHealthBar(true);
            hasActivatedCanvas = true;
        }
    }

    private void OnUpgradeSelected(bool selected)
    {
        upgradeSelected = selected;

        Init();
    }

    private void OnDestroy()
    {
        //audioManager.PlaySound(UnitTypeProperty, AudioManager.SoundType.Death, unitInit.audioSource);

        // We only want the player which has authority over this unit to:
        // 1. Remove itself from their UnitSelectManager
        // 2. Remove itself from their onMove event if necessary
        if (hasAuthority)
        {
            unitSelectManager.RemoveUnit(gameObject);

            if (unitSelect.IsSelected && unitSelect.IsMoveable)
                unitSelectManager.onMove -= unitSelect.MoveUnit;
        }
    }

    // This OnTriggerEnter is only used to add the enemies to our enemies list.
    private void OnTriggerEnter(Collider other)
    {
        //Ground layer, Unrelated layer
        if (other.transform.root.gameObject.layer == 19 || other.transform.root.gameObject.layer == 10)
            return;

        if (other.transform.root != transform.root)
            switch (unitType)
            {
                case UnitType.Worker:
                    break;
                case UnitType.GroundAntiGround:
                    if (other.CompareTag("UnitGround") || other.CompareTag("Building"))
                        enemies.Add(other.gameObject);
                    break;
                case UnitType.GroundAntiAir:
                    if (other.CompareTag("UnitAir"))
                        enemies.Add(other.gameObject);
                    break;
                case UnitType.GroundAntiGroundAir:
                    if (other.CompareTag("UnitGround") || other.CompareTag("Building") ||
                        other.CompareTag("UnitAir"))
                        enemies.Add(other.gameObject);
                    break;
                case UnitType.AirAntiGround:
                    if (other.CompareTag("UnitGround") || other.CompareTag("Building"))
                        enemies.Add(other.gameObject);
                    break;
                case UnitType.AirAntiAir:
                    if (other.CompareTag("UnitAir"))
                        enemies.Add(other.gameObject);
                    break;
                case UnitType.AirAntiGroundAir:
                    if (other.CompareTag("UnitGround") || other.CompareTag("Building") ||
                        other.CompareTag("UnitAir"))
                        enemies.Add(other.gameObject);
                    break;
            }
    }

    // We exit the collision with the enemy we will remove them from our enemies list.
    private void OnTriggerExit(Collider other)
    {
        enemies.Remove(other.gameObject);
    }

    #region Properties
    public int UnitHealth
    {
        get
        {
            return unitHealth;
        }
        private set
        {
            unitHealth = value;
        }
    }

    public int UnitMaxHealth => unitMaxHealth;

    public float UnitAttackRate => unitAttackRate;

    public int UnitAttackDamage => unitAttackDamage;

    public int UnitAttackRange => unitAttackRange;

    public int UnitProjectileSpeed => unitProjectileSpeed;

    public GameObject EnemyTarget
    {
        get;
        set;
    }

    public bool IsAttackMove
    {
        get
        {
            return isAttackMove;
        }
        set
        {
            isAttackMove = value;
        }
    }

    public bool IsDead
    {
        get;
        set;
    }

    public NetworkIdentity RootNetId
    {
        get
        {
            return rootNetId;
        }
        set
        {
            rootNetId = value;
        }
    }

    public UnitType UnitTypeProperty => unitType;

    public UpgradeType UpgradeTypeProperty
    {
        get
        {
            return upgradeType;
        }
        set
        {
            upgradeType = value;
            SetUpgrades();
        }
    }
    #endregion
}