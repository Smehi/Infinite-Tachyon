﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitAnimation : MonoBehaviour
{
    private enum Euleraxis
    {
        x,
        y,
        z,
        xy,
        xz,
        yz,
        xyz
    }

    private UnitCombat unitCombat;
    private UnitInit unitInit;
    private NavMeshAgent body;

    [SerializeField] private bool rotatableWeapons;

    [SerializeField, Tooltip("Use the cogs that rotate the weapon")]
    private GameObject[] weapons;

    [SerializeField, Range(0.1f, 1f)] private float rotateSpeed;

    [SerializeField, Tooltip("The axis the weaponcog rotates around")]
    private Euleraxis elAxis;


    [SerializeField, Tooltip("idle, move, attack")]
    private AnimationState[] animationStates;

    private AnimationState idleState, moveState, attackState, currentState;
    public GameObject tempTarget;

    public bool IsAttacking;

    // Use this for initialization
    void Start()
    {
        IsAttacking = false;
        unitCombat = GetComponent<UnitCombat>();
        unitInit = GetComponent<UnitInit>();
        body = GetComponent<NavMeshAgent>();

        for (int i = 0; i < animationStates.Length; i++)
        {
            var animationState = animationStates[i];

            if (animationState.state == "idle")
            {
                idleState = animationState;
            }

            if (animationState.state == "move")
            {
                moveState = animationState;
            }

            if (animationState.state == "attack")
            {
                attackState = animationState;
            }
        }
    }


    private void FixedUpdate()
    {
        if (!unitInit.isActiveAndEnabled)
        {
            unitInit.enabled = true;
        }

        if (IsIdle() && !IsAttacking)
        {
            PlayAnimationState(idleState);
        }
        else if (IsAttacking)
        {
            PlayAnimationState(attackState);
        }

        if (!IsIdle())
        {
            PlayAnimationState(moveState);
        }
    }
    
    //Used to play the animations
    private void PlayAnimations(Animation[] animations, float speed)
    {
        for (int i = 0; i < animations.Length; i++)
        {
            var animation = animations[i];
            animation.clip.frameRate = speed;
            if (animation.isPlaying) return;
            else animation.Play();
        }
    }

    //Used to play the particleSystems
    private void PlayParticleSystems(ParticleSystem[] particleSystems, float lifeTime)
    {
        for (int i = 0; i < particleSystems.Length; i++)
        {
            var particleSystem = particleSystems[i];

            var main = particleSystem.main;

            if (particleSystem.isPlaying) return;
            else
            {
                particleSystem.gameObject.SetActive(true);
                particleSystem.Play();
            }
        }
    }

    //Used to play the AnimationState
    private void PlayAnimationState(AnimationState animationState)
    {
        if (currentState != animationState)
        {
            StopCurrentAnimationState(currentState);
        }

        if (!animationState.isPlaying)
        {
            PlayAnimations(animationState.animations, animationState.animationSpeed);
            PlayParticleSystems(animationState.particleSystems, animationState.particleLifetime);
            currentState = animationState;
            animationState.isPlaying = true;
        }
    }

    //Use this to initiate attack animations
    public void SetAttackAnimation(GameObject target)
    {
        if (target != null)
        {
            RotateWeapons(weapons, target, rotateSpeed, elAxis);
        }
    }

    //Stop the current animation
    private void StopCurrentAnimationState(AnimationState animationState)
    {
        if (animationState == null) return;

        for (int i = 0; i < animationState.animations.Length; i++)
        {
            var animation = animationState.animations[i];
            if (!animation.isPlaying) return;
            animation.Stop();
        }

        for (int i = 0; i < animationState.particleSystems.Length; i++)
        {
            var particleSystem = animationState.particleSystems[i];

            if (!particleSystem.isPlaying) return;
            else
            {
                particleSystem.Stop();
                particleSystem.gameObject.SetActive(false);
            }
        }

        animationState.isPlaying = false;
    }


    //Checks whether an unit is moving or not
    public bool IsIdle()
    {
        //  Debug.Log(body.velocity.magnitude);
        if (body.velocity.magnitude > Mathf.Epsilon) return false;
        else
        {
            return true;
        }
    }


    /// <summary>
    /// Rotate weapons towards enemy
    /// </summary>
    /// <param name="weapons">List of weaponcogs</param>
    /// <param name="target">the actual enemy target</param>
    /// <param name="timeStep">the speed of rotation</param>
    /// <param name="axis">which axis should the weapon rotate around (x , y , z)</param>
    private void RotateWeapons(GameObject[] weapons, GameObject target, float timeStep, Euleraxis axis)
    {
        // if (rotatableWeapons)
        //{

        //Vector3 direction = (target.transform.localPosition - transform.localPosition);
        //Quaternion rotateTo = Quaternion.LookRotation(direction);
        //transform.rotation = Quaternion.Euler(rotateTo.x, rotateTo.y, rotateTo.z);

        transform.LookAt(target.transform);
        Quaternion quat = transform.rotation;
        quat.eulerAngles = new Vector3(0, transform.rotation.eulerAngles.y - 180, 0f);
        transform.rotation = quat;

        //for (int i = 0; i < weapons.Length; i++)
        //{
        //    var obj = weapons[i];
        //var targetPos = target.transform.parent.position;
        //targetPos.y = transform.position.y;
        //transform.LookAt(targetPos, Vector3.up);
        //obj.transform.rotation = Quaternion.RotateTowards(obj.transform.rotation, target.transform.rotation, timeStep);
        //Vector3 targetPosition = target.transform.position - obj.transform.position;
        //Quaternion rotation = Quaternion.LookRotation(targetPosition);
        //obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, rotation, Time.time * timeStep);
        //Debug.Log(obj.transform.rotation);

        //obj.transform.eulerAngles = GetEuler(obj.transform, axis);
        //}
        // }
    }

    IEnumerator Rotate(GameObject target)
    {
        Vector3 direction = (target.transform.localPosition - transform.localPosition);
        Quaternion rotateTo = Quaternion.LookRotation(direction);

        while (true)
        {
            transform.localRotation =
                Quaternion.RotateTowards(transform.localRotation, rotateTo, Time.deltaTime * 0.1f);
            if (Vector3.Angle(transform.forward, direction) < 1)
                break;

            yield return null;
        }
    }

    /// <summary>
    /// Returns needed euler for rotation
    /// </summary>
    /// <param name="trans">The transform of the object you want to rotate</param>
    /// <param name="axis">The axis you want the object to rotate around</param>
    /// <returns></returns>
    private Vector3 GetEuler(Transform trans, Euleraxis axis)
    {
        Vector3 euler;

        euler = new Vector3(trans.eulerAngles.x, 0, 0);
        switch (axis)
        {
            case Euleraxis.x:
            {
                euler = new Vector3(trans.eulerAngles.x, 0, 0);
            }
                break;
            case Euleraxis.y:
            {
                euler = new Vector3(0, trans.eulerAngles.y, 0);
            }
                break;
            case Euleraxis.z:
            {
                euler = new Vector3(0, 0, trans.eulerAngles.z);
            }
                break;
            case Euleraxis.xy:
            {
                euler = new Vector3(trans.eulerAngles.x, trans.eulerAngles.y, 0);
            }
                break;
            case Euleraxis.xz:
            {
                euler = new Vector3(trans.eulerAngles.x, 0, trans.eulerAngles.z);
            }
                break;
            case Euleraxis.yz:
            {
                euler = new Vector3(0, trans.eulerAngles.y, trans.eulerAngles.z);
            }
                break;
            case Euleraxis.xyz:
            {
                euler = new Vector3(trans.eulerAngles.x, trans.eulerAngles.y, trans.eulerAngles.z);
            }
                break;
        }

        return euler;
    }
}

//Animation state class with particle systems
[System.Serializable]
public class AnimationState
{
    public string state;
    public Animation[] animations;
    public ParticleSystem[] particleSystems;
    [Range(30, 240)] public float animationSpeed;
    [Range(.1f, 10f)] public float particleLifetime;
    public bool isPlaying = false;
}