﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;

public class UnitSelect : ObjectType
{
    [HideInInspector] public NetworkIdentity parentNetworkId;

    [SerializeField] private Type objectType;
    [SerializeField] private bool isMoveable = true;
    [SerializeField] private GameObject selectionProjector;
    [SerializeField] private GameObject parent;

    private NavMeshAgent unitAgent;
    private UnitSelectManager unitSelectManager;
    private UnitCombat unitCombat;
    private BuildingHealth buildingHealth;

    void Start()
    {
        Invoke("SetReferences", 0.1f);
    }

    private void SetReferences()
    {
        // If we don;t have authority over this object we can just disable this script
        if (!parent.GetComponent<NetworkIdentity>().hasAuthority)
        {
            this.enabled = false;
            return;
        }

        IsSelected = false;

        unitSelectManager = LobbyManager.instance.localPlayer.GetComponent<UnitSelectManager>();

        if (isMoveable)
        {
            selectionProjector = transform.parent.Find("SelectionMesh").gameObject;
            unitCombat = parent.GetComponent<UnitCombat>();

            // Fighter units are put into an extra list which is why there is a check for that here
            if (unitCombat.UnitTypeProperty != UnitCombat.UnitType.Worker)
            {
                unitSelectManager.SetInExistingList(parent, this, objectType);
            }
            else
            {
                unitSelectManager.SetInExistingList(parent, this, objectType);
            }
        }
        else
        {
            buildingHealth = parent.GetComponent<BuildingHealth>();
            unitSelectManager.SetInExistingList(parent, this, objectType);
        }

        unitAgent = parent.GetComponent<NavMeshAgent>();
        parentNetworkId = parent.GetComponent<NetworkIdentity>();
    }

    public void SelectUnit()
    {
        if (isMoveable)
        {
            selectionProjector.SetActive(true);
            unitCombat.ActivateHealthBar(true);

            // Register this unit for the onMove event
            unitSelectManager.onMove += MoveUnit;
        }
        else
        {
            buildingHealth.ActivateHealthBar(true);
        }

        IsSelected = true;
    }

    public void DeselectUnit()
    {
        if (isMoveable)
        {
            selectionProjector.SetActive(false);

            if (unitCombat.UnitHealth == unitCombat.UnitMaxHealth)
            {
                unitCombat.ActivateHealthBar(false);
            }

            // Remove this unit from the onMove event
            unitSelectManager.onMove -= MoveUnit;
        }
        else
        {
            if (buildingHealth.Health == buildingHealth.MaxHealth)
            {
                buildingHealth.ActivateHealthBar(false);
            }
        }

        IsSelected = false;
    }

    // Get the point from the onMove event and move the unit towards that point
    // Additionally it can be a attackMove
    public void MoveUnit(Vector3 point, bool isAttackMove, GameObject target)
    {
        // Buildings can't move so we only do this for units
        if (isMoveable)
        {
            Vector3 movePoint = point;
            movePoint.y = transform.position.y;

            unitAgent.SetDestination(movePoint);

            if (isAttackMove)
            {
                // Set the target of this unit manually
                unitCombat.EnemyTarget = target;
            }

            unitCombat.IsAttackMove = isAttackMove;
        }
    }

    public void MoveInFormation(Vector3 point)
    {
        // Buildings can't move so we only do this for units
        if (isMoveable)
        {
            Vector3 movePoint = point;
            movePoint.y = transform.position.y;

            unitAgent.SetDestination(movePoint);

            unitCombat.IsAttackMove = false;
        }
    }

    public bool IsSelected
    {
        get;
        private set;
    }

    public bool IsMoveable
    {
        get { return isMoveable; }
    }

    public Type ObjectType
    {
        get { return objectType; }
    }
}
