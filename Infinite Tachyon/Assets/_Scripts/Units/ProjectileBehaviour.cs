﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProjectileBehaviour : NetworkBehaviour
{
    [SerializeField] private GameObject impactSystem;

    private bool targetKilled;
    private Rigidbody projectileRigidbody;

    private Vector3 enemyPosition;
    private Vector3 projectileMovement;

    private UnitCombat enemyUnitCombat, instantiatorUnitCombat;
    private BuildingHealth buildingHealth;

    private void Start()
    {
        targetKilled = false;
        projectileRigidbody = GetComponent<Rigidbody>();

        // First check is if the target is a unit, else if it is a building
        ObjectType.Type enemyObjectType = EnemyTarget.transform.parent.GetComponentInChildren<UnitSelect>().ObjectType;

        if (enemyObjectType == ObjectType.Type.Worker ||
            enemyObjectType == ObjectType.Type.Fighter)
        {
            enemyUnitCombat = EnemyTarget.transform.parent.parent.GetComponent<UnitCombat>();
        }
        else if (enemyObjectType == ObjectType.Type.Building)
        {
            buildingHealth = EnemyTarget.transform.parent.parent.GetComponent<BuildingHealth>();
        }

        instantiatorUnitCombat = Instantiator.GetComponent<UnitCombat>();
    }

    private void FixedUpdate()
    {
        // Move towards the targetevery frame is there is one, else destroy the parent that contains all other projectiles with the same target
        if (EnemyTarget != null)
        {
            enemyPosition = EnemyTarget.transform.position;
            projectileMovement = new Vector3(enemyPosition.x - transform.position.x,
                                             enemyPosition.y - transform.position.y,
                                             enemyPosition.z - transform.position.z).normalized;

            projectileMovement *= UnitCombat.UnitProjectileSpeed;

            projectileRigidbody.velocity = projectileMovement;
        }
        else
        {
            Destroy(this.transform.parent.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == EnemyTarget)
        {
            GameObject impact = Instantiate(impactSystem, other.transform.position, impactSystem.transform.rotation, transform.parent);
            Destroy(impact, 1f);

            // Check if we have a enemyUnitCombat
            if (enemyUnitCombat != null)
            {
                enemyUnitCombat.CmdGetDamage(UnitCombat.UnitAttackDamage);

                // Check if the enemy is dead or if the enemyUnitCombat is gone now
                if (enemyUnitCombat.IsDead || enemyUnitCombat == null || enemyUnitCombat.UnitHealth <= 0)
                {
                    Destroy(transform.parent.gameObject);
                }
                else
                {
                    Destroy(this.gameObject);
                }
            }
            else if (buildingHealth != null)
            {
                buildingHealth.CmdGetDamage(UnitCombat.UnitAttackDamage);

                if (buildingHealth.IsDead || buildingHealth == null || buildingHealth.Health <= 0)
                {
                    Destroy(transform.parent.gameObject);
                }
                else
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }

    private void OnDestroy()
    {
        GetComponent<NetworkTransform>().sendInterval = 0;
    }

    public GameObject Instantiator { get; set; }

    public UnitCombat UnitCombat { get; set; }

    public GameObject EnemyTarget { get; set; }
}