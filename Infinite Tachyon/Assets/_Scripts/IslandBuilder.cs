﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;

public class IslandBuilder : NetworkBehaviour
{
    [Header("Islands")] [SerializeField] private GameObject[] islands;
    [Range(0, 100)] [SerializeField] private float chanceToSpawn;

    [Header("Outposts")] [SerializeField] private GameObject[] outposts;
    [Range(0, 100)] [SerializeField] private float chanceOutpost;
    [SerializeField] private float[] outpostVision;
    private Vector3 correction = new Vector3(0, -2f, 0);

    [Header("Misc")] [SerializeField] private GameObject navMeshObject;
    private SyncListChances chanceList = new SyncListChances();
    private UnitSpawner localSpawner, notLocalSpawner;
    [SyncVar] private bool alreadySpawned;

    private void Start()
    {
        localSpawner = LobbyManager.instance.localPlayer.GetComponent<UnitSpawner>();
        var childCount = this.transform.childCount;
        if (chanceList.Count <= 0)
        {
            for (int i = 0; i < childCount; i++)
            {
                Chances chance = new Chances();
                chance.islandChance = Random.Range(0, 100);
                chance.islandSizeIndex = Random.Range(0, islands.Length);
                chance.outpostChance = Random.Range(0, 100);
                chanceList.Add(chance);
            }
        }

        foreach (var surface in navMeshObject.GetComponents<NavMeshSurface>())
        {
            surface.BuildNavMesh();
        }

        if (!alreadySpawned)
        {
            alreadySpawned = true;
            Invoke("SpawnAll", .2f);
        }
    }

    public class SyncListChances : SyncListStruct<Chances>
    {
    }

    public void SpawnAll()
    {
        // if (transform.childCount != chanceList.Count) return;

        for (int i = 0; i < chanceList.Count; i++)
        {
            var chance = chanceList[i];

            if (chanceToSpawn != 0 && chance.islandChance <= chanceToSpawn)
            {
                // Spawn a random island
                var islandToSpawn = islands[chance.islandSizeIndex];
                var position = transform.GetChild(i).transform.position;
                position.y = islandToSpawn.transform.position.y;

                // On each island randomly spawn an outpost or not. The outpost is the same size as the island
                if (chance.outpostChance <= chanceOutpost)
                {
                    var outpostToSpawn = outposts[chance.islandSizeIndex];
                    var positionOutpost = transform.GetChild(i).transform.position;
                    positionOutpost.y = outpostToSpawn.transform.position.y;
                    localSpawner.CmdSpawnIslandsAndOutposts(islandToSpawn, position, outpostToSpawn, positionOutpost,
                        outpostVision[chance.islandSizeIndex], correction);
                }
                else
                {
                    localSpawner.CmdSpawnIslandsAndOutposts(islandToSpawn, position, null, Vector3.zero, 0f,
                        Vector3.zero);
                }
            }

            //Destroy(transform.GetChild(i).gameObject);
        }
    }

    private void SpawnOutposts(GameObject island, GameObject outpostObj, Vector3 positionOutpost, int currentIndex)
    {
        var outpost = Instantiate(outpostObj, positionOutpost, Quaternion.identity);

        OutpostBehaviour outpostBehaviour = outpost.GetComponent<OutpostBehaviour>();
        outpostBehaviour.vision.transform.localScale =
            new Vector3(outpostVision[currentIndex], 1, outpostVision[currentIndex]);
        outpost.transform.SetParent(island.transform);
        //outpostBehaviour.parent = island.transform;
        outpost.transform.position += correction;
    }

    public struct Chances
    {
        public int islandChance;
        public int islandSizeIndex;
        public int outpostChance;
    }
}