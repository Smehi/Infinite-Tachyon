﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class SpawnUnitsUI : NetworkBehaviour
{
    #region Singleton
    public static SpawnUnitsUI instance = null;
    #endregion

    public GameObject UnitSpawnCanvas;
    
    private UnitSpawner localSpawnScript;
    private BuildInputs buildingSpawnScript;
    [SerializeField] private Text resourceText;
    [SerializeField] private Text powerText;


    private PlayerResources resScript;
    private Transform buildingBase;

    public  GameObject groundTab;
    public  GameObject groundUI;
    public  GameObject groundSpecialUI;

    public  GameObject airTab;
    public  GameObject airUI;
    public  GameObject airSpecialTab;
    
    public GameObject infoBox;
    public Text infoText;
    public string[] texts;

    public  GameObject headquartersTab;
    public  GameObject headquartersUI;
    public  GameObject docksButton;
    public  GameObject airfieldButton;
    public  GameObject techlabButton;

    public  GameObject tabs;

    private float coolDown;
    private float amountToSpawn;

    private void Start()
    {
        // If there is no instance of this GameObject, make one
        if (instance == null)
        {
            instance = this;
        }

        //Find all the different UI's, save them and deactivate them
        for (int i = 0; i < UnitSpawnCanvas.transform.childCount; i++)
        {
            if (UnitSpawnCanvas.transform.GetChild(i).name == "GroundUI")
            {
                groundUI = UnitSpawnCanvas.transform.GetChild(i).gameObject;
                groundUI.SetActive(false);
            }
            if (UnitSpawnCanvas.transform.GetChild(i).name == "AirUI")
            {
                airUI = UnitSpawnCanvas.transform.GetChild(i).gameObject;
                airUI.SetActive(false);
            }            
            if (UnitSpawnCanvas.transform.GetChild(i).name == "HeadquartersUI")
            {
                headquartersUI = UnitSpawnCanvas.transform.GetChild(i).gameObject;
                headquartersUI.SetActive(false);
            }            
            if (UnitSpawnCanvas.transform.GetChild(i).name == "Background")
            {
                tabs = UnitSpawnCanvas.transform.GetChild(i).gameObject;
            }
            if (UnitSpawnCanvas.transform.GetChild(i).name == "InfoBox")
            {
                infoBox = UnitSpawnCanvas.transform.GetChild(i).gameObject;
                infoBox.SetActive(false);
            }
        }

        for (int i = 0; i < tabs.transform.childCount; i++)
        {
            if (tabs.transform.GetChild(i).name == "DocksTab")
            {
                groundTab = tabs.transform.GetChild(i).gameObject;
                groundTab.SetActive(false);
            }
            if (tabs.transform.GetChild(i).name == "AirfieldTab")
            {
                airTab = tabs.transform.GetChild(i).gameObject;
                airTab.SetActive(false);
            }
            if (tabs.transform.GetChild(i).name == "HeadquartersTab")
            {
                headquartersTab = tabs.transform.GetChild(i).gameObject;
                headquartersTab.SetActive(false);
            }
        }

        for (int i = 0; i < headquartersUI.transform.childCount; i++)
        {
            if (headquartersUI.transform.GetChild(i).name == "Building - Docks")
            {
                docksButton = headquartersUI.transform.GetChild(i).gameObject;
                docksButton.SetActive(false);
            }
            if (headquartersUI.transform.GetChild(i).name == "Building - Airfield")
            {
                airfieldButton = headquartersUI.transform.GetChild(i).gameObject;
                airfieldButton.SetActive(false);
            }
            if (headquartersUI.transform.GetChild(i).name == "Building - Techlab")
            {
                techlabButton = headquartersUI.transform.GetChild(i).gameObject;
                techlabButton.SetActive(false);
            }
        }

        for (int i = 0; i < airUI.transform.childCount; i++)
        {
            if (airUI.transform.GetChild(i).name == "Air Unit - GroundAir")
            {
                airSpecialTab = airUI.transform.GetChild(i).gameObject;
                airSpecialTab.SetActive(false);
            }
        }

        for (int i = 0; i < groundUI.transform.childCount; i++)
        {
            if (groundUI.transform.GetChild(i).name == "Ground Unit - GroundAir")
            {
                groundSpecialUI = groundUI.transform.GetChild(i).gameObject;
                groundSpecialUI.SetActive(false);
            }

        }


        if (!isLocalPlayer)
        {
            GetComponent<SpawnUnitsUI>().enabled = false;
            UnitSpawnCanvas.SetActive(false);
        }

        ///Get the scripts needed for spawning units and buildings
        ///Wait for the base to spawn to make sure you can acces all scripts
        Invoke("WaitForBase", 1f);
        
        if (LobbyManager.instance.localPlayer != null && localSpawnScript == null)
        {
            localSpawnScript = GetComponent<UnitSpawner>();
            resScript = GetComponent<PlayerResources>();
        }

    }

    void FixedUpdate()
    {
        //Display the amount of power and IT in the UI
        if (resourceText != null)
        {
            resourceText.text = resScript.AmountIt.ToString("n0");
        }

        if (powerText != null)
        {
            powerText.text = resScript.CurrentEnergy.ToString("n0") + " / " + resScript.MaxEnergy.ToString("n0");
        }
    }

    public void WaitForBase()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).CompareTag("MainBase"))
            {
                buildingBase = transform.GetChild(i);
              
                break;
            }
        }

        if (buildingBase != null && buildingSpawnScript == null)
        {
            buildingSpawnScript = buildingBase.GetComponent<BuildInputs>();
        }
    }
   
    /// <summary>
    /// Cooldown overlay on top of buttons that spawn units
    /// Also a number display of the amount of units in queue
    /// </summary>
   
    public float CoolDown
    {
        get
        {
            return coolDown;
        }

        set
        {
            coolDown = value;
        }
    }

    public float AmountToSpawn
    {
        get
        {
            return amountToSpawn;
        }

        set
        {
            amountToSpawn = value;
        }
    }


    //Display the infobox and get the right info string from the array depening on the unit type
    public void DisplayInfo(string unitType)
    {
        infoBox.SetActive(true);
        switch (unitType)
        {
            case "Air-air":
                {
                    infoText.text = texts[0];
                }
                break;
            case "Air-ground":
                {
                    infoText.text = texts[1];
                }
                break;
            case "Air-ground-Air":
                {
                    infoText.text = texts[2];
                }
                break;
            case "Ground-air":
                {
                    infoText.text = texts[3];
                }
                break;
            case "Ground-ground":
                {
                    infoText.text = texts[4];
                }
                break;
            case "Ground-ground-Air":
                {
                    infoText.text = texts[5];
                }
                break;
        } 
            
    }

    public void DisableInfo()
    {
        infoBox.SetActive(false);
    }

    //When a button is clicked spawn a unit or building with the name that is assigned to the button
    public void UISpawnUnit(string unitName)
    {
        localSpawnScript.SpawnUnit(unitName);
    }

    public void UISpawnBuilding(string buildingName)
    {
        buildingSpawnScript.SpawnBuilding(buildingName);
    }
}
