﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour {

    public GameObject mainMenuCanvas;
    public GameObject multiplayerMenu;
    public GameObject lobbyMenuCanvas;
    public GameObject tutorialCanvas;
    public InputField ipInput;

    public Text tutorialText;
    public string[] tutorials;
    public Image tutoImg;
    public Sprite[] tutoSprites;

    private string connectionType;

    public static CanvasManager instance = null;

    private Player_ID player_ID;

    private int tutorialNum;
    
    void Awake()
    {
        // If there is no instance of this GameObject, make one
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void StartGame()
    {
        mainMenuCanvas.SetActive(false);
        multiplayerMenu.SetActive(true);
    }

    /// <summary>
    /// Depending on the connection type connect as host or as client
    /// When client type is a host ask for an ip address
    /// When there is no ip adress given by the player it will use the local ipaddress
    /// </summary>
    /// <param name="connType"></param>
    public void ToLobbyScreen(string connType)
    {
        switch (connType)
        {
            case "Host":
                LobbyManager.instance.StartHost();
                connectionType = "Host";
                break;
            case "Client":
                LobbyManager.instance.networkAddress = ipInput.text == "" ? "localhost" : ipInput.text;
                LobbyManager.instance.StartClient();
                connectionType = "Client";
                break;
        }
        multiplayerMenu.SetActive(false);
        lobbyMenuCanvas.SetActive(true);
        
    }

    public void ToMainMenu()
    {
        multiplayerMenu.SetActive(false);
        tutorialCanvas.SetActive(false);
        mainMenuCanvas.SetActive(true);
    }

    //reset the tutorial to the first text and image 
    public void ToTutorial()
    {
        mainMenuCanvas.SetActive(false);
        tutorialCanvas.SetActive(true);
        tutorialText.text = tutorials[0];
        tutoImg.sprite = tutoSprites[0];
        tutorialNum = 0;
    }

    public void NextTutorial()
    {
        tutorialNum += 1;
        if(tutorialNum >= tutorials.Length)
        {
            tutorialCanvas.SetActive(false);
            mainMenuCanvas.SetActive(true);
        }
        else
        {
            tutorialText.text = tutorials[tutorialNum];
            tutoImg.sprite = tutoSprites[tutorialNum];
        }
        
    }

    public void ToMultiplayerSelect()
    {
        lobbyMenuCanvas.SetActive(false);
        multiplayerMenu.SetActive(true);
    }

    /// <summary>
    /// Send ready to begin message
    /// Disable the 'ready' button and activate 'unready' button 
    /// Unready does the oppisite
    /// </summary>
    public void ReadyUp()
    {
        LobbyManager.instance.GetLocalPlayer().SendReadyToBeginMessage();
        lobbyMenuCanvas.transform.GetChild(1).gameObject.SetActive(false);
        lobbyMenuCanvas.transform.GetChild(2).gameObject.SetActive(true);
       
    }

    public void UnReady()
    {
        LobbyManager.instance.GetLocalPlayer().SendNotReadyToBeginMessage();
        lobbyMenuCanvas.transform.GetChild(1).gameObject.SetActive(true);
        lobbyMenuCanvas.transform.GetChild(2).gameObject.SetActive(false);
        
    }

    public void Disconnect()
    {
        switch (connectionType)
        {
            case "Host":
                LobbyManager.instance.StopHost();
                break;
            case "Client":
                LobbyManager.instance.StopClient();
                break;
        }
        ToMultiplayerSelect();
    }
}
