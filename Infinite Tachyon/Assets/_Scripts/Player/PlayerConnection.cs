﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerConnection : NetworkBehaviour
{
    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        LobbyManager.instance.localPlayer = gameObject;
        
    }

    void Start()
    {
        if (!isLocalPlayer)
        {
            LobbyManager.instance.notLocalPlayer = gameObject;
        }
    }
}
