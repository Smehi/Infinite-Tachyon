﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraMovement : NetworkBehaviour
{
    [SerializeField] private float speed = 40f;
    [SerializeField] private float zoomAmount = 4.5f;
    [SerializeField] private Vector3 snapToBaseVector;

    private float mouseX;
    private float mouseY;
    private float cameraX;
    private float cameraZ;
    private bool canMoveCamera;
    private Vector3 actualForward;

    private Camera cam;
    private GameObject circle;
    private float circleRadius;

    private int currentZoom = 8;
    private readonly int minZoom = 0;
    private readonly int maxZoom = 10;

    private Transform actualParent;
    private Transform myBase;

    private int timesPressed;
    private float timePressed;
    private float timeToDoublePress = 0.25f;

    public RectTransform minimapTransform;

    private void Start()
    {
        canMoveCamera = true;
        cam = GetComponent<Camera>();
        Invoke("GetCacheVars", 0.5f);

        if (!GetComponentInParent<NetworkIdentity>().isLocalPlayer)
        {
            this.gameObject.SetActive(false);
        }

        // Rotating the camera on the x-axis makes it hard to get the right foward so the camera doesn't move up/down.
        // With a rotation of 45 degrees is easy by adding the forward and the up but when changing the rotation that doesn't work anymore.
        // There probably is a way to calculate it ourselves but this works and also math is hard.
        Vector3 oldRotation = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(new Vector3(0, oldRotation.y, 0));
        actualForward = transform.forward;
        transform.rotation = Quaternion.Euler(oldRotation);
    }

    void GetCacheVars()
    {
        circle = SceneCache.instance.water;
        circleRadius = circle.GetComponentInChildren<SphereCollider>().radius;
        actualParent = transform.parent;

        for (int i = 0; i < transform.root.childCount; i++)
        {
            if (transform.root.GetChild(i).CompareTag("MainBase"))
            {
                myBase = transform.root.GetChild(i);
                return;
            }
        }
    }

    private void Update()
    {
        mouseX = Input.mousePosition.x;
        mouseY = Input.mousePosition.y;
        cameraX = cam.transform.position.x;
        cameraZ = cam.transform.position.z;

        // Zoom in when we are scrolling up and aren't on the closest zoom level
        if (Input.mouseScrollDelta.y > 0 && currentZoom > minZoom)
        {
            cam.orthographicSize -= zoomAmount;
            currentZoom--;
        }

        // Zoom in when we are scrolling down and aren't on the farthest zoom level
        if (Input.mouseScrollDelta.y < 0 && currentZoom < maxZoom)
        {
            cam.orthographicSize += zoomAmount;
            currentZoom++;
        }

        if (Input.GetKeyDown(KeyCode.B) && myBase && actualParent)
        {
            timesPressed++;

            // This check is when we pressed 'B' twice and the time we pressed the second one was faster/equal to the tie to double press
            if (timesPressed > 1 && Time.time - timePressed <= timeToDoublePress)
            {
                timesPressed = 0;
                timePressed = 0;

                SetParentLongTerm();
            }
            // We check is when we pressed 'B' three times or when the press is later than the time to double press
            else if (timesPressed > 2 || Time.time - timePressed > timeToDoublePress)
            {
                timesPressed = 1;
                timePressed = Time.time;

                SetParentOneTime();
            }
        }
    }

    void FixedUpdate()
    {
        // Only move the camera when the cursor is insize the window
        if (mouseX >= 0 && mouseX <= Screen.width &&
            mouseY >= 0 && mouseY <= Screen.height &&
            canMoveCamera)
        {
            // MOVE RIGHT
            if (cameraX < circleRadius && cameraZ > -circleRadius)
            {
                if ((mouseX >= Screen.width - 25 && mouseX <= Screen.width) || Input.GetAxisRaw("Horizontal") > 0)
                {
                    // We want to move to the right from the camera's axis. This is the red transform arrow in the scene.
                    transform.position += transform.right * (Time.deltaTime * speed);
                }
            }

            // MOVE LEFT
            if (cameraX > -circleRadius && cameraZ < circleRadius)
            {
                if ((mouseX <= 25 && mouseX >= 0) || Input.GetAxisRaw("Horizontal") < 0)
                {
                    transform.position += -transform.right * (Time.deltaTime * speed);
                }
            }

            // MOVE FORWARDS
            if (cameraZ < circleRadius && cameraX < circleRadius)
            {
                if ((mouseY >= Screen.height - 25 && mouseY <= Screen.height) || Input.GetAxisRaw("Vertical") > 0)
                {
                    // Because the camera transform is rotated 45 degrees we can't use a single axis to move forwards/backwards.
                    // We have to add both the forward and up to get the actual forward we want.
                    transform.position += actualForward * (Time.deltaTime * speed);
                }
            }

            // MOVE BACKWARDS
            if (cameraZ > -circleRadius && cameraX > -circleRadius)
            {
                if ((mouseY <= 25 && mouseY >= 0) || Input.GetAxisRaw("Vertical") < 0)
                {
                    transform.position += -actualForward * (Time.deltaTime * speed);
                }
            }

            if (Input.GetKey(KeyCode.Mouse0))
            {
                transform.position = GetMinimapPosition();
            }
        }
    }

    public Vector3 GetMinimapPosition()
    {
        Vector3 minimapMousePosition = new Vector3();
        Vector3 minimapPosition = new Vector3();
        Vector3[] v = new Vector3[4];
        float scaler = 0;
        minimapTransform.GetWorldCorners(v);
        Vector3 minimapOrigin = (v[0] + v[2]) / 2; // Midle of the minimap
        if (Input.mousePosition.x > v[0].x && v[3].x > Input.mousePosition.x && Input.mousePosition.y < v[1].y && v[3].y < Input.mousePosition.y)
        {
            minimapMousePosition = Input.mousePosition - minimapOrigin;
            scaler = circleRadius / (minimapOrigin.x - v[0].x);
            minimapPosition.x = minimapMousePosition.x;
            minimapPosition.z = minimapMousePosition.y;
            minimapPosition *= scaler;
            minimapPosition.y = transform.position.y;
            minimapPosition.z -= 70;
        }
        else
        {
            minimapPosition = transform.position;
        }

        return minimapPosition;
    }

    // Quickly set the parent of the camera to the base and move it slightly so it's in view
    // Put it back under the playerConnection afterwards
    private void SetParentOneTime()
    {
        canMoveCamera = true;

        transform.SetParent(myBase);
        transform.localPosition = snapToBaseVector;
        transform.SetParent(actualParent);
    }

    // Set the parent of the camera to the base and leave it there so it follows the base
    private void SetParentLongTerm()
    {
        canMoveCamera = false;

        transform.SetParent(myBase);
        transform.localPosition = snapToBaseVector;
    }
}