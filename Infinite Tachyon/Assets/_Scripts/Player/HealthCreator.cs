﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.Networking;

public class HealthCreator : NetworkBehaviour
{
    [SerializeField] private GameObject healthQuad;
    [SerializeField] private BoxCollider healthBox;
    [SerializeField, Range(5, 20)] private int amountBlocks;
    [SerializeField] private int amountGreenBlocks;

    public Color currentHealth;
    private List<MeshRenderer> meshRends;
    private MaterialPropertyBlock propBlock;

    [SyncVar] private int maxHealth;


    // Use this for initialization
    void Start()
    {
        propBlock = new MaterialPropertyBlock();
       

        if (healthBox != null)
        {
            transform.position = new Vector3(healthBox.bounds.min.x, healthBox.bounds.max.y, healthBox.bounds.max.z);
        }
        else
        {
            healthBox = transform.parent.Find("HealthBox").gameObject.GetComponent<BoxCollider>();
            transform.position = new Vector3(healthBox.bounds.min.x, healthBox.bounds.max.y, healthBox.bounds.max.z);
        }
        transform.localEulerAngles = new Vector3(90,-180,0);
    }

    /// <summary>
    /// Creates a healthBar with quads and fills a list with the meshRenderer of them
    /// </summary>
    /// <param name="maxHealth">Max health of the unit or building</param>
    public void InitializeHealth(int maxHealth)
    {
        MaxHealth = maxHealth;

        if (meshRends != null)
        {//List is already created before
            //Check if list is already filled
            if (meshRends.Count > 0) return;
        }

        meshRends = new List<MeshRenderer>();

        for (int i = 0; i < amountBlocks; i++)
        {
            healthQuad.transform.localScale = new Vector3(1f, .75f, .75f);
            Vector3 pos = new Vector3(0, 0, 0);

            GameObject healthPoint = Instantiate(healthQuad, pos, transform.rotation, this.transform);

            if (healthBox != null)
            {
                float newSize = (healthBox.bounds.max.x - healthBox.bounds.min.x) / amountBlocks;
                healthPoint.transform.localScale = new Vector3(newSize, .75f, .75f);
                healthPoint.transform.localPosition = new Vector3((-i * newSize), 0, 0);
            }

            meshRends.Add(healthPoint.GetComponent<MeshRenderer>());
        }

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Resets the healthBar with a new max health
    /// </summary>
    /// <param name="newHealth">The new max health</param>
    public void ResetHealthBar(int newHealth)
    {
        if (meshRends != null && meshRends.Count > 0)
        {
            for (int i = 0; i < meshRends.Count; i++)
            {
                var healthPoint = meshRends[i].gameObject;

                Destroy(healthPoint);
            }

            meshRends.Clear();
        }

        InitializeHealth(newHealth);
    }

    /// <summary>
    /// Change the quad colors with materialPropertyBlocks
    /// Depending on the amount of healthPoints
    /// </summary>
    /// <param name="healthPoints">amount of current healthPoints of an unit or building</param>
    public void ChangeHealth(float healthPoints)
    {
        if (healthPoints <= 0)
        {
            amountGreenBlocks = 0;
            return;
        }

        amountGreenBlocks = (int) Mathf.Ceil(healthPoints / maxHealth * amountBlocks);

        int propId = Shader.PropertyToID("_Color");

        if (healthPoints <= maxHealth / 4)
        {
            currentHealth = Color.red;
        }
        else if (healthPoints <= maxHealth / 2)
        {
            currentHealth = Color.yellow;
        }
        else
        {
            currentHealth = Color.green;
        }

        if (meshRends != null && meshRends.Count > 0)
        {
            //Clear the color of the quads
            for (int i = meshRends.Count - 1; i > amountGreenBlocks - 1; i--)
            {
                propBlock = new MaterialPropertyBlock();
                propBlock.SetColor(propId, Color.clear);
                meshRends[i].SetPropertyBlock(propBlock);
            }

            //Change the color of the quads
            for (int i = 0; i < amountGreenBlocks; i++)
            {
                if (!meshRends[i].gameObject.activeInHierarchy)
                {
                    continue;
                }

                propBlock = new MaterialPropertyBlock();

                propBlock.SetColor(propId, currentHealth);
                meshRends[i].SetPropertyBlock(propBlock);
            }
        }
    }

    public int MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }
}