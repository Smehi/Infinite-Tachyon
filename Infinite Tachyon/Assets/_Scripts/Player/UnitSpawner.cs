﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class UnitSpawner : NetworkBehaviour
{
    public Unit[] units;
    [SerializeField] private Queue<Unit> groundQueue, airQueue, workerQueue;
    private Transform parentUnits;
    private int currentIndex;
    private PlayerResources resScript;
    private SpawnUnitsUI spawnUIScript;
    private BaseConnection baseConnectScript;

    [Range(1f, 5f), SerializeField, Tooltip("The cooldown reduction per spawn building")]
    private float coolDownReduction;

    private void Start()
    {
        if (!isLocalPlayer)
        {
            GetComponent<UnitSpawner>().enabled = false;
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == "Units")
            {
                parentUnits = transform.GetChild(i);
                break;
            }
        }

        workerQueue = new Queue<Unit>();
        groundQueue = new Queue<Unit>();
        airQueue = new Queue<Unit>();
        baseConnectScript = GetComponent<BaseConnection>();
        spawnUIScript = GetComponent<SpawnUnitsUI>();
        resScript = GetComponent<PlayerResources>();
    }


    private void Update()
    {
        TrainingQueue(airQueue);
        TrainingQueue(groundQueue);
        TrainingQueue(workerQueue);
    }

    /// <summary>
    /// Trains all units in the unitQueue
    /// </summary>
    /// <param name="unitQueue">The queue for the units</param>
    private void TrainingQueue(Queue<Unit> unitQueue)
    {
        if (unitQueue.Count > 0)
        {
            var unit = unitQueue.Peek();

            if (unit.needToSpawn)
            {
                if (unit.coolDown.x > Mathf.Epsilon)
                {
                    float IT_perSecond = (unit.IT_required / unit.coolDown.y);

                    if (unit.trainingInterval > 0 && resScript.HasSufficientIt(IT_perSecond))
                    {
                        unit.trainingInterval -= Time.deltaTime;
                        unit.coolDown.x -= Time.deltaTime;
                    }

                    TrainUnit(unit, IT_perSecond);
                }
                else
                {
                    if (unit.amountToSpawn > 0)
                    {
                        CmdSpawnUnit(unit.name);
                        unitQueue.Dequeue();
                        unit.amountToSpawn--;
                        if (unit.amountToSpawn > 0)
                        {
                            //Train the unit 
                            unit.coolDown.x = unit.maxCoolDown;
                            unit.decreaseValue = unit.IT_required;
                        }
                    }
                    else
                    {
                        unit.amountToSpawn = 0;
                        unit.needToSpawn = false;
                    }
                }

                spawnUIScript.CoolDown = (unit.coolDown.x) / (unit.coolDown.y);
                unit.coolDownImage.fillAmount = spawnUIScript.CoolDown;
                spawnUIScript.AmountToSpawn = unit.amountToSpawn;
                unit.unitAmount.text = spawnUIScript.AmountToSpawn.ToString();
            }
        }
    }

    private void TrainUnit(Unit unit, float decrement)
    {
        unit.isTraining = (unit.decreaseValue > 0);

        if (unit.isTraining && resScript.HasSufficientIt(decrement))
        {
            if (unit.trainingInterval <= 0f)
            {
                if (unit.decreaseValue > 0)
                {
                    unit.trainingInterval = 1f;
                    unit.decreaseValue -= decrement;
                    resScript.Decrease_IT(decrement);
                }
                else
                {
                    unit.decreaseValue = 0;
                }
            }
        }
    }

    /// <summary>
    /// Allows spawning the unit
    /// </summary>
    /// <param name="name">Unit name</param>
    public void SpawnUnit(string name)
    {
        Unit spawnedUnit = SearchForUnit(name);
        if (spawnedUnit != null)
        {
            if (spawnedUnit.maxCoolDown <= Mathf.Epsilon && baseConnectScript.amountDocks.Count <= 0
                || baseConnectScript.amountAirfields.Count <= 0)
            {
                spawnedUnit.maxCoolDown = spawnedUnit.coolDown.y;
            }

            if (spawnedUnit.amountToSpawn <= 0)
            {
                //    spawnedUnit.amountToSpawn++;
                if (spawnedUnit.type == Unit.UnitType.Air)
                {
                    spawnedUnit.maxCoolDown = spawnedUnit.maxCoolDown -
                                              ((spawnedUnit.maxCoolDown / 100 * coolDownReduction)
                                               * baseConnectScript.amountAirfields.Count);
                    spawnedUnit.coolDown.x = spawnedUnit.maxCoolDown;
                }
                else
                {
                    spawnedUnit.maxCoolDown = spawnedUnit.maxCoolDown -
                                              ((spawnedUnit.maxCoolDown / 100 * coolDownReduction)
                                               * baseConnectScript.amountDocks.Count);
                    spawnedUnit.coolDown.x = spawnedUnit.maxCoolDown;
                }
            }

            spawnedUnit.decreaseValue = spawnedUnit.IT_required;
            spawnedUnit.amountToSpawn++;
            spawnedUnit.needToSpawn = true;
            spawnUIScript.AmountToSpawn = spawnedUnit.amountToSpawn;
            spawnedUnit.unitAmount.text = spawnUIScript.AmountToSpawn.ToString();

            switch (spawnedUnit.type)
            {
                case Unit.UnitType.Air:
                    airQueue.Enqueue(spawnedUnit);
                    break;
                case Unit.UnitType.Ground:
                    groundQueue.Enqueue(spawnedUnit);
                    break;
                case Unit.UnitType.Worker:
                    workerQueue.Enqueue(spawnedUnit);
                    break;
            }
        }
    }

    /// <summary>
    /// Search for the unit by name
    /// </summary>
    /// <param name="name">Unit name</param>
    /// <returns>Unit</returns>
    private Unit SearchForUnit(string name)
    {
        Unit unitObject = null;
        foreach (Unit item in units)
        {
            if (item.name == name)
                unitObject = item;
        }

        return unitObject;
    }

    /// <summary>
    /// Instantiate the unit and spawn the unit on the network
    /// </summary>
    /// <param name="unitName"></param>
    [Command]
    public void CmdSpawnUnit(string unitName)
    {
        switch (unitName)
        {
            case "air_Air":
                currentIndex = 0;
                break;
            case "air_Ground":
                currentIndex = 1;
                break;
            case "air_GroundAir":
                currentIndex = 2;
                break;
            case "ground_Air":
                currentIndex = 3;
                break;
            case "ground_Ground":
                currentIndex = 4;
                break;
            case "ground_GroundAir":
                currentIndex = 5;
                break;
            case "ground_GroundWorker":
                currentIndex = 6;
                break;
        }

        // How the upgrade order works:
        // First the host sets the Upgrade type for the unit here.
        // This property gets changed only on the host. In the property a method is called that updates all the values
        // for that unit with the new upgrades. At the end of that method a boolean is set, this boolean is synced over
        // the network and hooked onto a method, so that method will be called by all clients. 
        // In this method the only thing that happens is that Init is called.
        // Init was the Start method but it has to be executed after the upgrades are applied.

        GameObject currentUnit = Instantiate(units[currentIndex].gameObject, parentUnits);
        UnitInit unitInitScript = currentUnit.GetComponent<UnitInit>();
        UnitCombat unitCombatScript = currentUnit.GetComponent<UnitCombat>();
        unitCombatScript.UpgradeTypeProperty = UnitCombat.UpgradeType.None;
        unitInitScript.Parent = this.gameObject;
        NetworkServer.SpawnWithClientAuthority(currentUnit, connectionToClient);
    }

    public Vector2 GetCooldown(Unit unit)
    {
        Vector2 coolDown;

        coolDown = unit.coolDown;

        return coolDown;
    }

    public float GetUnitAmount(Unit unit)
    {
        float unitAmount;

        unitAmount = unit.amountToSpawn;

        return unitAmount;
    }


    [Command]
    public void CmdSpawnIslandsAndOutposts(GameObject islandToSpawn, Vector3 islandPosition, GameObject outpostObj , Vector3 positionOutpost , float outpostVision ,
        Vector3 correction)
    {
        if (islandToSpawn == null) return;

        var island = Instantiate(islandToSpawn , islandPosition , Quaternion.identity);
        island.transform.SetParent(SceneCache.instance.water.transform);
        island.transform.Rotate(0 , Random.Range(0 , 360) , 0);
        NetworkServer.Spawn(island);
        if (outpostObj == null)
            return;

        var outpost = Instantiate(outpostObj , positionOutpost , Quaternion.identity);

        OutpostBehaviour outpostBehaviour = outpost.GetComponent<OutpostBehaviour>();
        outpostBehaviour.vision.transform.localScale =
            new Vector3(outpostVision , 1 , outpostVision);
        outpost.transform.SetParent(island.transform);
        //outpostBehaviour.parent = island.transform;
        outpost.transform.position += correction;
        NetworkServer.Spawn(outpost);
    }

    [Command]
    public void CmdSpawnOutposts(GameObject island, GameObject outpostObj, Vector3 positionOutpost, float outpostVision,
        Vector3 correction)
    {
        if (outpostObj == null) return;

        var outpost = Instantiate(outpostObj, positionOutpost, Quaternion.identity);

        OutpostBehaviour outpostBehaviour = outpost.GetComponent<OutpostBehaviour>();
        outpostBehaviour.vision.transform.localScale =
            new Vector3(outpostVision, 1, outpostVision);
        outpost.transform.SetParent(island.transform);
        //outpostBehaviour.parent = island.transform;
        outpost.transform.position += correction;
        NetworkServer.Spawn(outpost);
    }
}

[System.Serializable]
public class Unit
{
    public GameObject gameObject;
    public UnitType type;
    public string name;
    public Vector2 coolDown;
    public Image coolDownImage;
    public Text unitAmount;
    public bool needToSpawn;
    public int amountToSpawn;
    public int IT_required;
    public float decreaseValue;
    public float maxCoolDown;
    [HideInInspector] public float trainingInterval;
    public bool isTraining = false;


    public enum UnitType
    {
        Air,
        Ground,
        Worker
    }
}