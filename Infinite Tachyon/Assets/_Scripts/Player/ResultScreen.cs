﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreen : MonoBehaviour
{
    private List<string> winnerTextMessages = new List<string>();
    private List<string> loserTextMessages = new List<string>();

    private Text wonOrLostText;
    private bool hasWon;

    // This method loads all the win and lose messages into and array
    // Doing is like this allows us to use intorpolated strings to acces the player names for more personalized messages
    private void LoadStrings()
    {
        winnerTextMessages.Add($"Congratulations {LobbyManager.instance.playerName}, you won the game!");
        winnerTextMessages.Add($"Outstanding moves, {LobbyManager.instance.playerName}!");
        winnerTextMessages.Add($"{LobbyManager.instance.playerName}, you've got 200IQ.");
        winnerTextMessages.Add($"Good job annihilating {LobbyManager.instance.enemyName}, {LobbyManager.instance.playerName}!");
        winnerTextMessages.Add($"Let's get this bread, {LobbyManager.instance.playerName}!");
        winnerTextMessages.Add($"Stop googling strategy guides.");
        winnerTextMessages.Add($"GG EZ.");
        winnerTextMessages.Add($"Winner Winner Turkey Dinner!!");
        winnerTextMessages.Add($"Git gud, {LobbyManager.instance.enemyName}.");

        loserTextMessages.Add($"Better luck next time, {LobbyManager.instance.playerName}!");
        loserTextMessages.Add($"Get fucked, {LobbyManager.instance.playerName}!");
        loserTextMessages.Add($"{LobbyManager.instance.enemyName} is victorious... you are not.");
        loserTextMessages.Add($"Go to bed, kiddo.");
        loserTextMessages.Add($"Try to keep your buildings alive next time...");
        loserTextMessages.Add($"Try playing with your monitor turned on.");
        loserTextMessages.Add($"{LobbyManager.instance.enemyName} has a bigger dick than you");
        loserTextMessages.Add($"Mad?");
        loserTextMessages.Add($"You shoould try fighting back.");
    }

    private void DisplayResult()
    {
        // Set the timeScale to 0, so the game basically stops its physics engine
        Time.timeScale = 0;
        int index;

        transform.parent.Find("UnitCanvas").gameObject.SetActive(false);
        transform.parent.Find("GameEndCanvas").gameObject.SetActive(true);
        wonOrLostText = transform.parent.Find("GameEndCanvas/WinOrLoseText").GetComponent<Text>();

        if (HasWon)
        {
            index = UnityEngine.Random.Range(0, winnerTextMessages.Count);
            wonOrLostText.color = Color.green;
            wonOrLostText.text = winnerTextMessages[index];
        }
        else
        {
            index = UnityEngine.Random.Range(0, loserTextMessages.Count);
            wonOrLostText.color = Color.red;
            wonOrLostText.text = loserTextMessages[index];
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public bool HasWon
    {
        get
        {
            return hasWon;
        }
        set
        {
            // We only call LoadString and DisplayResult when HasWon is set
            hasWon = value;
            LoadStrings();
            DisplayResult();
        }
    }
}
