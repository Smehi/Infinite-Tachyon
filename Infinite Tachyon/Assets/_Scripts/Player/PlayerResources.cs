﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class PlayerResources : NetworkBehaviour
{
    //PlayerResources
    [SerializeField] private float amountIt;
    [SerializeField] private float maxEnergy;
    [SerializeField] private float currentEnergy;

    private void Start()
    {
        if (!isLocalPlayer)
        {
            GetComponent<PlayerResources>().enabled = false;
        }
    }

    public void Decrease_IT(float increment)
    {
        if (HasSufficientIt(increment))
        {
            amountIt -= increment;
        }
    }

    public bool HasSufficientEnergy(float energyCost)
    {
        return currentEnergy + energyCost <= maxEnergy;
    }

    public bool HasSufficientIt(float itCost)
    {
        return amountIt - itCost >= 0 && HasSufficientEnergy((0));
    }

    public float AmountIt
    {
        get
        {
            return amountIt;
        }
        set
        {
            amountIt = value;
        }
    }

    public float MaxEnergy
    {
        get
        {
            return maxEnergy;
        }
        set
        {
            maxEnergy = value;
        }
    }

    public float CurrentEnergy
    {
        get
        {
            return currentEnergy;
        }
        set
        {
            currentEnergy = value;
        }
    }
}