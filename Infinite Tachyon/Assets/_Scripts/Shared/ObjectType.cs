﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectType : MonoBehaviour
{
    public enum Type
    {
        Worker,
        Fighter,
        Building
    }
}
