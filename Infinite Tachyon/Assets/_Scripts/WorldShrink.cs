﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(NetworkIdentity))]
public class WorldShrink : NetworkBehaviour
{
    [Header("Shrinking")]
    [SerializeField] private float shrinkInterval;
    [SerializeField] private float timeBeforeShrinkForWarning;
    [SerializeField] private float minimalWorldScaleOnXAndZ;
    [SerializeField] private int shrinkSessionsCount;
    [SerializeField] private float shrinkTime;

    private float nextShrink;
    private float deltaScale;
    private float shrinkagePerSession;
    private float shrinkSpeed;
    private bool canShrink = true;
    private GameObject world;
    private IEnumerator currentShrinkCoroutine;
    private IEnumerator currentTimerCoroutine;
    private int timesShrunk;

    private bool iconChangedNormal;
    private bool iconChangedWarning;
    private bool iconChangedShrink;

    private bool shrinkWarningPlayed = false;

    [Header("Timer UI")]
    [SerializeField] private Sprite timerIconBeforeShrinking;
    [SerializeField] private Sprite timerIconWarningBeforeshrinking;
    [SerializeField] private Sprite timerIconWhileShrinking;

    private Image timerIcon;
    private Text timerText;

    // Use this for initialization
    private void Start()
    {
        // Disable the WorldShrink and UI on the other player because that fixes issues.
        Invoke("DisableOther", 0.5f);

        timerIcon = transform.Find("ShrinkTimerCanvas/ShrinkIcon").GetComponent<Image>();
        timerText = transform.Find("ShrinkTimerCanvas/ShrinkText").GetComponent<Text>();

        world = SceneCache.instance.water;

        // Find the difference in the scale from the current scale and the minimum scale that we want.
        // Then devide this by the amount of times we want the world to shrink.
        // This gets us a value of how much the world will shrink per session.
        // At last we want to get the speed of the shrink.
        // We can rearrange the formula: distance = speed * time
        // to a new formula: speed = distance / time.
        deltaScale = world.transform.localScale.x - minimalWorldScaleOnXAndZ;
        shrinkagePerSession = deltaScale / shrinkSessionsCount;
        shrinkSpeed = shrinkagePerSession / shrinkTime;

        nextShrink = Time.time + shrinkInterval;

        if (isServer && hasAuthority)
        {
            RpcChangeTimeOnClient(shrinkInterval, true, false, false);
            currentTimerCoroutine = TimerBeforeShrinking(shrinkInterval);
            StartCoroutine(currentTimerCoroutine);
        }
    }

    // Disable the other timer is you are the server
    // If not the server you disable your own timer because the server does the timer and is syncen on the other object
    private void DisableOther()
    {
        if (isServer)
        {
            if (!hasAuthority)
            {
                GetComponent<WorldShrink>().enabled = false;
                transform.Find("ShrinkTimerCanvas").gameObject.SetActive(false);
            }
        }
        else
        {
            if (hasAuthority)
            {
                GetComponent<WorldShrink>().enabled = false;
                transform.Find("ShrinkTimerCanvas").gameObject.SetActive(false);
            }
        }
    }

    private void FixedUpdate()
    {
        // The client that is the server will make an RPC call to all other client to tell them to start shrinking their world.
        // RPC calls are only made on the server, so we must first check if the current client is also the server.
        if (isServer)
        {
            if (Time.fixedTime >= nextShrink - timeBeforeShrinkForWarning && !shrinkWarningPlayed)
            {
                StartCoroutine(FindObjectOfType<AudioManager>().LoopSound(10, "ShrinkWarning", FindObjectOfType<AudioManager>().audioSourceWorld));

                shrinkWarningPlayed = true;
            }

            if (Time.fixedTime >= nextShrink && canShrink)
            {
                canShrink = false;
                RpcCallShrink();
            }
        }
    }

    [ClientRpc]
    private void RpcCallShrink()
    {
        // We set the desired scale here by subtracting the shrink per session value from the current scale.
        // The y value of the scale can stay the same.
        Vector3 desiredScale;
        desiredScale.x = world.transform.localScale.x - shrinkagePerSession;
        desiredScale.y = world.transform.localScale.y;
        desiredScale.z = world.transform.localScale.z - shrinkagePerSession;

        // If there is for some reason already a shrinking coroutine in progress we want that one to stop and start a new one.
        if (currentShrinkCoroutine != null)
        {
            StopCoroutine(currentShrinkCoroutine);
        }

        print("Starting coroutine!");
        currentShrinkCoroutine = Shrink(world.transform.localScale, desiredScale, shrinkTime);
        StartCoroutine(currentShrinkCoroutine);
    }

    private IEnumerator Shrink(Vector3 worldScale, Vector3 desiredworldScale, float time)
    {
        // I got this t part from an answer and don't understand it but it works.
        float t = 0;

        // Play audio here
        StartCoroutine(FindObjectOfType<AudioManager>().LoopSound(2, "Shrink", FindObjectOfType<AudioManager>().audioSourceWorld));

        while (t < 1)
        {
            t += Time.deltaTime / time;
            world.transform.localScale = Vector3.Lerp(worldScale, desiredworldScale, t);

            yield return null;
        }

        timesShrunk++;

        // Remove the script if we have shrunk the max amount of times.
        if (timesShrunk >= shrinkSessionsCount)
        {
            transform.Find("ShrinkTimerCanvas").gameObject.SetActive(false);
            Destroy(this);
        }

        nextShrink = Time.time + shrinkInterval;
        canShrink = true;
        shrinkWarningPlayed = false;
    }

    private IEnumerator TimerBeforeShrinking(float time)
    {
        timerIcon.sprite = timerIconBeforeShrinking;

        while (time > 0)
        {
            if (time <= timeBeforeShrinkForWarning)
            {
                RpcChangeTimeOnClient(time, false, true, false);
            }
            else
            {
                RpcChangeTimeOnClient(time, true, false, false);
            }

            time--;

            yield return new WaitForSeconds(1);
        }

        if (currentTimerCoroutine != null)
        {
            StopCoroutine(currentTimerCoroutine);
        }

        currentTimerCoroutine = TimerWhileShrinking(shrinkTime);
        StartCoroutine(currentTimerCoroutine);
    }

    private IEnumerator TimerWhileShrinking(float time)
    {
        timerIcon.sprite = timerIconWhileShrinking;

        while (time > 0)
        {
            RpcChangeTimeOnClient(time, false, false, true);

            time--;

            yield return new WaitForSeconds(1);
        }

        if (currentTimerCoroutine != null)
        {
            StopCoroutine(currentTimerCoroutine);
        }

        currentTimerCoroutine = TimerBeforeShrinking(shrinkInterval);
        StartCoroutine(currentTimerCoroutine);
    }

    [ClientRpc]
    private void RpcChangeTimeOnClient(float time, bool normal, bool warning, bool shrinking)
    {
        if (timerIcon && timerText)
        {
            // Set the right timerIcon
            if (normal && !iconChangedNormal)
            {
                timerIcon.sprite = timerIconBeforeShrinking;
                iconChangedNormal = true;
                iconChangedWarning = false;
                iconChangedShrink = false;
            }
            else if (warning && !iconChangedWarning)
            {
                timerIcon.sprite = timerIconWarningBeforeshrinking;
                iconChangedNormal = false;
                iconChangedWarning = true;
                iconChangedShrink = false;
            }
            else if (shrinking && !iconChangedShrink)
            {
                timerIcon.sprite = timerIconWhileShrinking;
                iconChangedNormal = false;
                iconChangedWarning = false;
                iconChangedShrink = true;
            }

            // Set the right timerText
            int minutes;
            int seconds;

            minutes = Mathf.FloorToInt(time / 60);
            seconds = (int)(time % 60);

            timerText.text = $"{minutes.ToString("00")}:{seconds.ToString("00")}";
        }
    }
}
