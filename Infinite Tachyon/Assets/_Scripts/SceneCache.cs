﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneCache : MonoBehaviour {

    #region Singleton
    public static SceneCache instance = null;
    #endregion

    public GameObject water;
    public GameObject navMesh;
    public Vector3 cameraRotation;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        water = GameObject.Find("Water");
        navMesh = GameObject.Find("NavMesh");
    }
}
