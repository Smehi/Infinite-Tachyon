﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyNames : NetworkBehaviour {

    private GameObject networkManager;
    [SyncVar] private string playerName;
    private Text textComponent;
    LobbyManager lobbyManager;

	
	void Start () {
        networkManager = GameObject.Find("NetworkManager");
        lobbyManager = networkManager.GetComponent<LobbyManager>();
        textComponent = GetComponent<Text>();

        playerName = "Player" + NetworkServer.connections.Count;
        CmdChangePlayerName(playerName);
        textComponent.text = playerName;
    }

    
    public void CmdChangePlayerName(string newName)
    {
        Debug.Log(newName);
        playerName = newName;
        textComponent.text = playerName;
    }
}
