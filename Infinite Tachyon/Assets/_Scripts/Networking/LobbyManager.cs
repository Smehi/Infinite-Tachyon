﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyManager : NetworkLobbyManager
{
    #region Singleton

    public static LobbyManager instance = null;

    #endregion

    private NetworkManager networkManager;

    [Header("Countdown")] public float prematchCountdown;
    public bool startedCoroutine = false;
    private IEnumerator currentRunningCoroutine;

    [Header("UI Elements")] public Text player1Name;
    public Text player2Name;
    public Text inputField;
    public Text readyLocalPlayer;
    public Text readyNotLocalPlayer;
    public Text countdownText;

    [Header("DropDown")] public Dropdown dropDown;
    public GameObject dropDownCreator;
    public Sprite[] colorSprites;
    [ColorUsageAttribute(true,true)] public Color[] colors;


    [Header("Player")] public string playerName;
    public string enemyName;
    public int playerColor, enemyColor;
    private NetworkLobbyPlayer lobbyPlayer;
    public GameObject localPlayer;
    public GameObject notLocalPlayer;


    private void Awake()
    {
        // If there is no instance of this GameObject, make one
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        networkManager = GetComponent<NetworkManager>();
    }

    // Checks if the players are ready and sets the text accordingly
    private void Update()
    {
        for (var i = 0; i < lobbySlots.Length; i++)
        {
            if (lobbySlots[i] == null)
                break;
            if (lobbySlots[i].readyToBegin)
            {
                if (lobbySlots[i].isLocalPlayer)
                {
                    readyLocalPlayer.text = "Ready!";
                }
                else
                {
                    if (readyLocalPlayer != null)
                        readyNotLocalPlayer.text = "Ready!";
                }
            }
            else
            {
                if (lobbySlots[i].isLocalPlayer)
                {
                    readyLocalPlayer.text = "Waiting...";
                    if (startedCoroutine)
                    {
                        StopCoroutine(currentRunningCoroutine);
                        startedCoroutine = false;
                        (lobbySlots[i] as LobbyPlayer).RpcResetCountdown();
                    }
                }
                else
                {
                    readyNotLocalPlayer.text = "Waiting...";
                    if (startedCoroutine)
                    {
                        StopCoroutine(currentRunningCoroutine);
                        startedCoroutine = false;
                        (lobbySlots[i] as LobbyPlayer).RpcResetCountdown();
                    }
                }
            }
        }
    }

    public NetworkLobbyPlayer GetLocalPlayer()
    {
        NetworkLobbyPlayer netPl = null;

        for (var i = 0; i < lobbySlots.Length; i++)
            if (lobbySlots[i].isLocalPlayer)
            {
                netPl = lobbySlots[i] as NetworkLobbyPlayer;
                break;
            }

        if (netPl == null)
            Debug.Log("no local players");

        return netPl;
    }

    public void OnNameChange()
    {
        playerName = inputField.text;
    }

    // creates dropdown for collor selection
    public void CreateDropDown()
    {
        if (!dropDownCreator.activeInHierarchy)
        {
            return;
        }

        dropDownCreator.SetActive(false);
        dropDown.ClearOptions();

        var colorData = new List<Dropdown.OptionData>();

        foreach (var clrSpr in colorSprites)
        {
            var colorOpt = new Dropdown.OptionData(clrSpr.name, clrSpr);
            colorData.Add(colorOpt);
        }

        dropDown.AddOptions(colorData);
    }

    public void ChangeColor()
    {
        playerColor = dropDown.value;
    }

    // Checks if all players are ready when a player readys up
    public override void OnLobbyServerPlayersReady()
    {
        var allready = true;
        for (var i = 0; i < lobbySlots.Length; ++i)
            if (lobbySlots[i] != null)
                allready &= lobbySlots[i].readyToBegin;

        if (allready)
        {
            if (currentRunningCoroutine != null)
                StopCoroutine(currentRunningCoroutine);
            currentRunningCoroutine = ServerCountdownCoroutine();
            StartCoroutine(currentRunningCoroutine);
            startedCoroutine = true;
        }
    }

    // if players are ready start countdown befor game starts
    public IEnumerator ServerCountdownCoroutine()
    {
        var remainingTime = prematchCountdown;
        var floorTime = Mathf.FloorToInt(remainingTime);

        while (remainingTime > 0)
        {
            yield return null;

            remainingTime -= Time.deltaTime;
            var newFloorTime = Mathf.FloorToInt(remainingTime);

            if (newFloorTime != floorTime)
            {
                //updates the countdown every second with the clients
                floorTime = newFloorTime;

                for (var i = 0; i < lobbySlots.Length; ++i)
                    if (lobbySlots[i] != null)
                        (lobbySlots[i] as LobbyPlayer).RpcUpdateCountdown(floorTime);
            }
        }

        for (var i = 0; i < lobbySlots.Length; ++i)
            if (lobbySlots[i] != null)
                (lobbySlots[i] as LobbyPlayer).RpcUpdateCountdown(0);
        ServerChangeScene(playScene);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        transform.GetChild(0).gameObject.SetActive(true);
    }

    // deactivates the lobby players in the play scene
    public override void OnLobbyClientSceneChanged(NetworkConnection conn)
    {
        base.OnLobbyClientSceneChanged(conn);
        readyNotLocalPlayer.gameObject.SetActive(false);
        readyLocalPlayer.gameObject.SetActive(false);
    }
}