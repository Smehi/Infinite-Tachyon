﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// has the rpcs to update the count down or reset

public class LobbyPlayer : NetworkLobbyPlayer
{
    [SyncVar] public bool isActive = true;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (isLocalPlayer)
            LobbyManager.instance.localPlayer = this.gameObject;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name != "DemoScene_Lobby")
        {
            gameObject.SetActive(false);
        }
    }

    [ClientRpc]
    public void RpcUpdateCountdown(int countdown)
    {
        LobbyManager.instance.countdownText.text = "Match Starting in " + countdown;
        LobbyManager.instance.countdownText.gameObject.SetActive(countdown != 0);
    }

    [ClientRpc]
    public void RpcResetCountdown()
    {
        LobbyManager.instance.countdownText.text = "Match Canceled";
        StopCoroutine(LobbyManager.instance.ServerCountdownCoroutine());
    }
}
