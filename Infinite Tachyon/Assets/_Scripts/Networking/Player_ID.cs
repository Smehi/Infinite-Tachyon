﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

// Gets player name from the lobbymanager and sets the name to the right text(same for the color the player selected)
public class Player_ID : NetworkBehaviour
{
    [SyncVar] public string playerUniqueName;
    [SyncVar] public int playerUniqueColor;
    private NetworkInstanceId playerNetID;
    private Transform myTransform;
    public Text playerName;
    public Text otherPlayerName;
    public Transform canvas;
    public Image playerColor, otherPlayerColor;
   
    public override void OnStartLocalPlayer()
    {
        GetNetIdentity();
        SetIdentity();
    }

    // Use this for initialization
    void Awake()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (myTransform.name == "NetworkLobbyPlayer(Clone)")
        {
            SetIdentity();
        }
    }

    void GetNetIdentity()
    {
        playerNetID = GetComponent<NetworkIdentity>().netId;
        CmdTellServerID(GetUniqueName());
        CmdSetColor(GetPlayerColor());
    }

    void SetIdentity()
    {
        if (!isLocalPlayer)
        {
            otherPlayerName.text = playerUniqueName;
            otherPlayerColor.sprite = LobbyManager.instance.colorSprites[playerUniqueColor];
            LobbyManager.instance.enemyName = playerUniqueName;
            LobbyManager.instance.enemyColor = playerUniqueColor;
            playerName.gameObject.SetActive(false);
        }
        else
        {
            playerName.text = playerUniqueName;
            playerColor.sprite = LobbyManager.instance.colorSprites[playerUniqueColor];
            LobbyManager.instance.playerName = playerUniqueName;
            LobbyManager.instance.playerColor = playerUniqueColor;
            otherPlayerName.gameObject.SetActive(false);
        }
    }

    string GetUniqueName()
    {
        return LobbyManager.instance.playerName;
    }

    int GetPlayerColor()
    {
        return LobbyManager.instance.playerColor;
    }

    [Command]
    void CmdTellServerID(string uniqueName)
    {
        playerUniqueName = uniqueName;
    }

    [Command]
    void CmdSetColor(int pColor)
    {
        playerUniqueColor = pColor;
    }
}
