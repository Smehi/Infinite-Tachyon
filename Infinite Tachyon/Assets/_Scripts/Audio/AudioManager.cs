﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private SoundType soundType;
    public enum SoundType
    {
        Engine,
        Build,
        Shoot,
        Death,
        Gather
    }

    public Sound[] sounds;

    public static AudioManager instance;

    public AudioSource audioSourceWorld;
    public AudioSource musicSource;

    private UnitCombat unitCombat;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        audioSourceWorld = this.gameObject.AddComponent<AudioSource>();
        musicSource = this.gameObject.AddComponent<AudioSource>();
        unitCombat = FindObjectOfType<UnitCombat>();

        DontDestroyOnLoad(gameObject);

        StartCoroutine(Play("MainMusic", musicSource));
    }

    public void MusicChange(string clip)
    {
        string currentClip;
        currentClip = clip;

        if (clip != currentClip)
        {
            StartCoroutine(Play(clip, musicSource));
        }
    }

    public void PlaySound(UnitCombat.UnitType unitType, SoundType soundType, AudioSource source)
    {
        switch (soundType)
        {
            case SoundType.Engine:
                switch (unitType)
                {
                    case UnitCombat.UnitType.Worker:
                        StartCoroutine(Play("GroundUnitHeavyTravel", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGround:
                        StartCoroutine(Play("GroundUnitLightTravel", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiAir:
                        StartCoroutine(Play("GroundUnitLightTravel", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGroundAir:
                        StartCoroutine(Play("GroundUnitLightTravel", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGround:
                        StartCoroutine(Play("AirUnitHeavyTravel", source));
                        break;
                    case UnitCombat.UnitType.AirAntiAir:
                        StartCoroutine(Play("AirUnitLightTravel", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGroundAir:
                        StartCoroutine(Play("AirUnitHeavyTravel", source));
                        break;
                    default:
                        break;
                }
                break;
            case SoundType.Build:
                switch (unitType)
                {
                    case UnitCombat.UnitType.Worker:
                        StartCoroutine(Play("GroundUnitBuild", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGround:
                        StartCoroutine(Play("GroundUnitBuild", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiAir:
                        StartCoroutine(Play("GroundUnitBuild", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGroundAir:
                        StartCoroutine(Play("GroundUnitBuild", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGround:
                        StartCoroutine(Play("AirUnitBuild", source));
                        break;
                    case UnitCombat.UnitType.AirAntiAir:
                        StartCoroutine(Play("AirUnitBuild", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGroundAir:
                        StartCoroutine(Play("AirUnitBuild", source));
                        break;
                    default:
                        break;
                }
                break;
            case SoundType.Shoot:
                switch (unitType)
                {
                    case UnitCombat.UnitType.Worker:
                        break;
                    case UnitCombat.UnitType.GroundAntiGround:
                        StartCoroutine(Play("MachineGun", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiAir:
                        StartCoroutine(Play("MachineGun", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGroundAir:
                        StartCoroutine(Play("ArtilleryAttack", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGround:
                        StartCoroutine(Play("MachineGun", source));
                        break;
                    case UnitCombat.UnitType.AirAntiAir:
                        StartCoroutine(Play("MachineGun", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGroundAir:
                        StartCoroutine(Play("ArtilleryAttack", source));
                        break;
                    default:
                        break;
                }
                break;
            case SoundType.Death:
                switch (unitType)
                {
                    case UnitCombat.UnitType.Worker:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGround:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiAir:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGroundAir:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGround:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    case UnitCombat.UnitType.AirAntiAir:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    case UnitCombat.UnitType.AirAntiGroundAir:
                        StartCoroutine(Play("UnitDeath", source));
                        break;
                    default:
                        break;
                }
                break;
            case SoundType.Gather:
                switch (unitType)
                {
                    case UnitCombat.UnitType.Worker:
                        StartCoroutine(Play("UnitGather", source));
                        break;
                    case UnitCombat.UnitType.GroundAntiGround:
                        break;
                    case UnitCombat.UnitType.GroundAntiAir:
                        break;
                    case UnitCombat.UnitType.GroundAntiGroundAir:
                        break;
                    case UnitCombat.UnitType.AirAntiGround:
                        break;
                    case UnitCombat.UnitType.AirAntiAir:
                        break;
                    case UnitCombat.UnitType.AirAntiGroundAir:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    public IEnumerator Play(string name, AudioSource source)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source = source;

        s.source.clip = s.clip;
        s.source.volume = s.volume;
        s.source.playOnAwake = s.playOnAwake;
        s.source.loop = s.loop;
        s.source.spatialBlend = s.spacial_blend;
        s.source.outputAudioMixerGroup = s.output;

        s.source.rolloffMode = s.rolloffMode;

        s.source.dopplerLevel = s.dopplerLevel;
        s.source.spread = s.spread;
        s.source.maxDistance = s.maxDistance;

        s.source.Play();

        yield return new WaitForSeconds(s.clip.length);
    }

    public IEnumerator LoopSound(int amount, string name, AudioSource source)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source = source;
        
        for (int i = 0; i < amount; i++)
        {
            s.source.PlayOneShot(s.clip, s.volume);
            s.source.Play();

            yield return new WaitForSeconds(s.clip.length);
        }
    }

    public SoundType SoundTypeProperty
    {
        get { return soundType; }
    }
}

[System.Serializable]
public class Sound
{
    public string name;

    public AudioClip clip;
    public AudioMixerGroup output;

    [Range(0f, 1f)]
    public float volume;
    [Range(0f, 1f)]
    public float spacial_blend;

    public bool loop;
    public bool playOnAwake;

    [Header("3D Sound Settings")]
    [Range(0f, 1f)]
    public float dopplerLevel;
    public AudioRolloffMode rolloffMode;

    [Range(0f, 1f)]
    public float spread;
    public int maxDistance;

    [HideInInspector]
    public AudioSource source;
}
