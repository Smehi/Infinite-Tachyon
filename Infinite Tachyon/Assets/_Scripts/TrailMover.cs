﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrailMover : MonoBehaviour
{
    [SerializeField] private float startPositionX;
    [SerializeField] private float endPositionX;
    private UnitAnimation unitAnimation;

    private void Start()
    {
        unitAnimation = GetComponentInParent<UnitAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!unitAnimation.IsIdle())
        {
            float xPos = Mathf.Lerp(startPositionX, endPositionX, Mathf.PingPong(Time.time, 1f));
            transform.localPosition = new Vector3(xPos, transform.localPosition.y, transform.localPosition.z);
        }
    }
}