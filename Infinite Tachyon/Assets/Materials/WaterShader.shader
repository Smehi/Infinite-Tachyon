﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Custom/WaterShader" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_Glossiness("Smoothness",Range(0.01,1)) = 0
		[PowerSlider(5.0)] _Shininess("Shininess", Range(0.01, 1)) = 0.078125
		_MainTex("Base (RGB) TransGloss (A)", 2D) = "white" {}
		[Normal][NoScaleOffset] _NormalMap("NormalMap",2D) = "bump"{}
		_Amplitude("Amplitude", Float) = 1
		_WaveLength("WaveLength", Float) = 10
		_Speed("Speed", Float) = 1
		_BumpScale("BumpScale", Float) = 1
		_EmissionMap("EmissionMap", 2D) = "black"{}
		[HDR]_EmissionColor("Emission Color", Color) =(1,1,1)
	}

		SubShader{
			Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
			LOD 300

		CGPROGRAM
		//#pragma surface surf BlinnPhong alpha:fade
		//#pragma surface surf BlinnPhong vertex:vert alpha:fade addshadow
		#include "UnityPBSLighting.cginc"
		#pragma surface surf StandardSpecular vertex:vert alpha:fade addshadow

		sampler2D _MainTex,_NormalMap,_EmissionMap;
		fixed4 _Color, _EmissionColor;
		half _Shininess, _Glossiness;
		float _Amplitude, _WaveLength, _Speed;
		fixed _BumpScale;

		half4 LightingStandardSpecular(SurfaceOutputStandardSpecular s, half3 lightDir, half atten) {
			half NdotL = dot(s.Normal, lightDir);
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten);
			c.a = s.Alpha;
			return c;
		}

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
			float2 uv_EmissionMap;
			float3 uv_Illum;
		};

	

		void vert(inout appdata_full vertexData) 
		{
			float3 pos = vertexData.vertex.xyz;

			float k = 2 * UNITY_PI / _WaveLength;
			float f = k * (pos.x - _Speed * _Time.y);

			pos.y = _Amplitude * sin(f);

			float3 tangent = normalize(float3(1, k * _Amplitude * cos(f), 0));
			float3 normal = float3(-tangent.y, tangent.x, 0);

			vertexData.vertex.xyz = pos;
			vertexData.normal = normal;
		}

	

		void surf(Input IN, inout SurfaceOutputStandardSpecular o) {
			
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * _Color.rgb;
			o.Normal = fixed4(_BumpScale, _BumpScale, 1.0, 1.0) * UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			/*normal = normal * _BumpScale;
			o.Normal = normalize(normal);*/
			o.Alpha =  _Color.a;
			o.Smoothness = _Glossiness;
			o.Emission = _EmissionColor.rgb * tex2D(_EmissionMap, IN.uv_EmissionMap);
		}
		ENDCG

	}
		Fallback "Legacy Shaders/Transparent/VertexLit"
		CustomEditor "WaterShaderGUI"
}
