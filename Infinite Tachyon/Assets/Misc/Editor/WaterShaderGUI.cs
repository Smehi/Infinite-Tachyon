﻿using UnityEngine;
using UnityEditor;

public class WaterShaderGUI : ShaderGUI
{

    MaterialEditor materialEditor;
    MaterialProperty[] materialProperties;
    GUIContent newLabel = new GUIContent();

    public override void OnGUI(MaterialEditor editor, MaterialProperty[] properties)
    {
        //base.OnGUI(materialEditor, properties);

        materialEditor = editor;
        materialProperties = properties;
        DoMain();
    }

    void DoMain()
    {
        GUILayout.Label("Maps", EditorStyles.boldLabel);

        MaterialProperty mainTex = FindProp("_MainTex");
        materialEditor.TexturePropertySingleLine(MakeLabel(mainTex, "Albedo"), mainTex, FindProp("_Color"));
       
        DoNormals();
        DoSmoothness();
        DoEmission();
        DoWaveProps("_Speed", "_Amplitude", "_WaveLength");
        materialEditor.TextureScaleOffsetProperty(mainTex);
    }

    void DoNormals()
    {
        MaterialProperty normalMap = FindProp("_NormalMap");
        materialEditor.TexturePropertySingleLine(MakeLabel(normalMap, "NormalMap"), normalMap, FindProp("_BumpScale"));
        materialEditor.TextureScaleOffsetProperty(normalMap);
    }

    void DoSmoothness()
    {
        MaterialProperty smooth = FindProp("_Glossiness");
        materialEditor.RangeProperty(smooth, "Smoothness");
    }

    void DoEmission()
    {
        MaterialProperty emission = FindProp("_EmissionMap");
        materialEditor.TexturePropertySingleLine(MakeLabel(emission, "Emission"), emission, FindProp("_EmissionColor"));
        materialEditor.TextureScaleOffsetProperty(emission);
    }

    void DoWaveProps(params string[] props )
    {
        for (int i = 0; i < props.Length; i++)
        {
            MaterialProperty waveProps = FindProp(props[i]);
            materialEditor.FloatProperty(waveProps, props[i]);
        }
       
    }

    MaterialProperty FindProp(string _shaderName)
    {
        return FindProperty(_shaderName, materialProperties);
    }

    GUIContent MakeLabel(MaterialProperty prop, string toolTip = null)
    {
        newLabel.text = prop.displayName;
        newLabel.text = toolTip;
        return newLabel;
    }
}
